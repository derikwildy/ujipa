<script>


function OnSubmit()
		{
			 if (confirm('Apakah anda yakin?')) {
					$( "#delete_detail" ).submit();
					} else {
					return false;
					} 
			}
</script>

    <div class="col-lg-12">
        
       <div class="row">
        <div class="col-md-12">
        <ol class="breadcrumb">
        
            <div class="col-md-6" id="headercol">
                
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  Participant Registration</li>
            </div>
        
            <!-- INPUTAN SEARCH -->
            <div class="col-md-6" id="topcol">
                <form class="form-horizontal" name="input_data" action="home.php?menu=PESERTA" method="post">
                  <div class="form-group col-md-11 pull-right">
                          
                          
                           <tr >
                                
                           
                            <input type="text" name="txtSearchNIP" class="form-control col-md-5" id="txtSearchNIP" placeholder=" NIP">
                            <input type="text" name="txtSearchNama" class="form-control col-md-6" id="txtSearchNama" placeholder="Nama Peserta">
                            <button class="btn btn-info"><span class="fa fa-search" id="searchsp"></span></button>
                            </tr>
                          
                  </div>
                </form>
            </div>
           
        </ol>
    </div>
</div>
        
        
        <div class="row">
            <?php
                if(isset($_REQUEST['msg'])){
            ?>
            <div class="col-md-10">
                  <div class="alert alert-warning alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div id="notifadmin"><?php echo $_REQUEST['msg'];?></div>
                </div>
            </div>
            <?php }?>
            
            <div class="col-lg-2 pull-right" id="insertbtnm">
                  <?php if($_SESSION['role'] == "adminpusat"){ ?>
                        <a href="javascript:lightbox2(null, 'view/upload-excel-peserta.php')">
                                    <button class="pull-right btn btn-success btn-sm"><img src="assets/img/excel.png" /></button>
                        </a>
                        <a href="javascript:lightbox2(null, 'view/tambah-peserta.php')">
                                    <button class="pull-right btn btn-primary btn-sm right-mg"><i class="fa fa-plus"></i> </button>
                        </a>
                        
                    <?php }?>
                 
                 
            </div>
        </div>
        
    
        
 <div class="row">   
    <div class="table-responsive col-md-12" id="txtsmall" style="overflow-x: auto">   
        <table class="table table-striped table-hover">
            <thead id="headercol">
            <tr >
                <td>No</td>
                <td>NIP</td>
                <td>Nama Peserta</td>
                <td>Jabatan</td>
                <td>Tanggal Lahir</td>
                <td>Email</td>
                <td>HP</td>
                <td>Pend.</td>
                <td>Unit</td>
                <td>Cabang</td>
                <td>Rayon</td>
                <td>Ang.</td>
                <td>Kelas</td>
            </tr>
            </thead>
            
            	<?php
					
            $dataPerPage = 20;
			$pageNo = 1;
				if(isset($_REQUEST['pageNo']))
				{
					$pageNo = $_REQUEST['pageNo'];
				}
                
				$offset = ($pageNo - 1) * $dataPerPage;
			
							
                    //SEARCH YA INI
                    $search = '';
                    $search2 = '';
                    $where = "";
                    if(!empty($_POST['txtSearchNama'])){
                        $search = $_POST['txtSearchNama'];
                    }
                    if(!empty($_POST['txtSearchNIP'])){
                        $search2 = $_POST['txtSearchNIP'];
                    }

                    $where = " WHERE MP.NIP != '' ";
                    if($search != ''){
                        $where .= " AND MP.Nama LIKE '%".$search."%' ";
                    }
                    if($search2 != ''){
                        $where .= " AND MP.NIP LIKE '%".$search2."%' ";
                    }
							$sql = "select *, MU.UnitName 'UnitNames', MK.NamaKelas 'NamaKelass' FROM mspegawai MP INNER JOIN mskelas MK ON MP.KelasID=MK.KelasID INNER JOIN msunit MU ON MP.UnitID=MU.UnitID INNER JOIN mscabang MC ON MP.CabangID=MC.CabangID INNER JOIN msrayon MR ON MP.RayonID=MR.RayonID ".$where." ORDER BY Nama ASC LIMIT $offset, $dataPerPage";


							$result = mysql_query($sql);
							
						$no=0;
								while($row = mysql_fetch_array($result))
								{
							$no++;
							
?>


            
     
            <tr class="active" >
                <td><?php echo $no;?></td>
                <td><?php echo $row['NIP'];?></td>
                <td><?php echo $row['Nama'];?></td>
                <td><?php echo $row['Jabatan'];?></td>
                <td><?php echo $row['TanggalLahir'];?></td>
                <td><?php echo $row['Email'];?></td>
                <td><?php echo $row['HP'];?></td>
                <td><?php echo $row['Pendidikan'];?></td>
                <td><?php echo $row['UnitNames'];?></td>
                <td><?php echo $row['NamaCabang'];?></td>
                <td><?php echo $row['NamaRayon'];?></td>
                <td><?php echo $row['Angkatan'];?></td>
                <td><?php echo $row['NamaKelass'];?></td>
                
                <?php if($_SESSION['role'] == "adminpusat"){ ?>
                <td>
                   
                    <a href="javascript:lightbox(null, 'view/edit-detail-peserta.php?&NIP=<?php echo $row['NIP'];?>&newmenu=EE1F2F1')">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-pencil"></i>
                            </button>
                    </a>
                </td>
                
                <td>
                    <a id="delete_detail" onclick="return OnSubmit()" href="controller/doDelete-detail-peserta.php?NIP=<?php echo $row['NIP'];?>">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-trash-o"></i>
                            </button>
                    </a>
                </td>
                
                <?php }?>
            </tr>
        <?php }?>
            
         
          
        </table>
        </div>
        
        <div style="text-align: center;">
                <?php
    		$count = 0;
				$queryProduct = "select count(*) 'count'  FROM mspegawai MP INNER JOIN mskelas MK ON MP.KelasID=MK.KelasID INNER JOIN msunit MU ON MP.UnitID=MU.UnitID";
				$rsProduct = mysql_query($queryProduct);
				
				$rowProduct = mysql_fetch_array($rsProduct);
				
				$count = $rowProduct[0];
				
				$totalPage = ceil($count/$dataPerPage);

				?>
				<div class="row">
					<div class="col-xs-12" align="center">
						
						<ul class="pagination">
						<?php				
							for($i = 1; $i <= $totalPage; $i++)
							{
								if($i == $pageNo)
								{
									echo"<li class=\"active\"><a>$i</a></li>";
								}
								else
								{?>
									<li><a align='center' href='home.php?menu=PESERTA&pageNo=<?php echo $i;?>'><?php echo $i;?></a></li>
								<?php	}
								
								echo "&nbsp;";
							}
						?>
						</ul>
					</div>
				</div>
                </div>
        
     
     </div>
        </div>
  