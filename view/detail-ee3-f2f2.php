<?php include("../connect.php");
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<script>


function OnSubmit()
		{
			
			 if (confirm('Apakah anda yakin?')) {
					$( "#delete_detail" ).submit();
					} else {
					return false;
					} 
			
				 
			}
</script>

</head>

<body>
<div class="conpopup">
<div class="col-md-12" id="conbottom">
<h2 id="txttitle"><i class="fa fa-search"></i> View | Detail Face to Face 2</h2>
<table class="table table-striped table-hover huruf">
  <thead id="headercol">
        <tr>
            <td>No</td>
            <td>NIP</td>
            <td>NAMA PESERTA</td>
            <td>JUDUL PA</td>
            <td>ANGKATAN</td>
            <td>MENTOR</td>
        </tr>
 </thead>
    <?php 
        $qs = "select MP.NIP AS NIP, MP.Nama AS NamaPeserta, DK.Judul AS Judul, MM.Nama AS NamaMentor , HK.HeaderKelasID 'HeaderKelasID', DK.AngkaAngkatan AS AngkaAngkatan from TrHeaderKelas HK INNER JOIN TrDetailKelas DK ON HK.HeaderKelasID = DK.HeaderKelasID 
        INNER JOIN MsPegawai MP ON DK.NIP=MP.NIP INNER JOIN msmentor MM ON DK.NIM=MM.NIM WHERE DK.HeaderKelasID='".$_REQUEST['id']."' ORDER BY  MP.Nama ASC";
        $query = mysql_query($qs);
        $no=0;
        while($row = mysql_fetch_array($query)){
        
        $no++;
    ?>
  	<tbody>
    	<tr class="active">
                <td><?php echo $no;?></td>
                <td><?php echo $row['NIP'];?></td>
                <td><?php echo $row['NamaPeserta'];?></td>
                <td><?php echo $row['Judul'] ?></td>
                <td><?php echo $row['AngkaAngkatan'] ?></td>
                <td><?php echo $row['NamaMentor'] ?></td>
                <td>
                    <?php if($_SESSION['role']=="admin"){ ?>
                    <a href="home.php?menu=EDITDETAIL&NIP=<?php echo $row['NIP'];?>&HeaderKelasID=<?php echo $row['HeaderKelasID']?>&newmenu=EE3F2F2">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-pencil"></i>
                            </button>
                    </a>
                    <?php } ?>
                </td>
                
                <td>
                    <?php if($_SESSION['role']=="admin"){ ?>
                    <a id="delete_detail" onclick="return OnSubmit()" href="controller/doDelete-detail.php?NIP=<?php echo $row['NIP'];?>&HeaderKelasID=<?php echo $row['HeaderKelasID']?>&newmenu=EE3F2F2">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-trash-o"></i>
                            </button>
                    </a>
                    <?php } ?>
                </td>
                
    	</tr>
    </tbody>
    <?php }?>
</table> 
<?php if($_SESSION['role']=="admin"){ ?>
<form action="home.php?menu=NEWDETAIL2&id=<?php echo $_REQUEST['id'];?>&newmenu=EE3F2F2" method="post">
       <p id="txtnumb" >Masukan jumlah peserta yang akan ditambah : </p>
     <div class="form-group input-group">
        
                            <span class="input-group-addon"><img src="assets/img/num.png">  </span>
                            <input type="text" class="form-control" name="txtno" id="txtno" placeholder="[1..10]" style="width: 5em">
        
    </div>
    <input type="hidden" name="kelas" id="kelas" value="<?php echo $_REQUEST['kelas'];?>"/>
    <input type="hidden" name="id" id="id" value="<?php echo $_REQUEST['id'];?>"/>
    <input type="submit" class="btn btn-primary btn-sm" value="Add Data"/>

</form>
<?php } ?>
</div>
</div>

</body>
</html>