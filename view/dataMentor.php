<script>


function OnSubmit()
		{
			
			 if (confirm('Apakah anda yakin?')) {
					$( "#delete_detail" ).submit();
					} else {
					return false;
					} 
			
				 
			}
</script>
 	
    
    <div class="col-lg-12">
            
    <div class="row">
        <div class="col-md-12">
        <ol class="breadcrumb">
        
            <div class="col-md-6" id="headercol">
                
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  Mentor Registration</li>
            </div>
        
             <!-- INPUTAN SEARCH -->
            <div class="col-md-6" id="topcol">
                <form class="form-horizontal" name="input_data" action="home.php?menu=MENTOR" method="post">
                  <div class="form-group col-md-11 pull-right">
                          
                          
                            <tr>
                                
                          
                            <input type="text" name="txtSearchNIP" class="form-control col-md-5" id="txtSearchNIP" placeholder="NIP">
                            <input type="text" name="txtSearchNama" class="form-control col-md-6" id="txtSearchNama" placeholder="Nama  Mentor">
                            <button class="btn btn-info"><span class="fa fa-search" id="searchsp"></span></button>
                            <tr>
                          
                  </div>
                </form>
            </div>
           
        </ol>
    </div>
</div>
        
        
         
        
        
        
        <div class="row">
            <?php
                if(isset($_REQUEST['msg'])){
            ?>
            <div class="col-md-10">
                  <div class="alert alert-warning alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div id="notifadmin"><?php echo $_REQUEST['msg'];?></div>
                </div>
            </div>
            <?php }?>
        
             <div class="col-lg-2 pull-right" id="insertbtnm">
                  <?php if($_SESSION['role'] == "adminpusat"){ ?>
                        <a href="javascript:lightbox2(null, 'view/upload-excel-mentor.php')">
                                    <button class="pull-right btn btn-success btn-sm"><img src="assets/img/excel.png" /></button>
                        </a>
                        <a href="javascript:lightbox2(null, 'view/tambah-mentor.php')">
                                    <button class="pull-right btn btn-primary btn-sm right-mg"><i class="fa fa-plus"></i> </button>
                        </a>
                        
                    <?php }?>
                 
                 
            </div>
        </div>
       
    <div class="row">
       <div class="table-responsive col-md-12" id="center" style="overflow-x: auto">     
        <table class="table table-striped table-hover" >
            <thead id="headercol">
            <tr >
                <td>No</td>
                <td>NIP</td>
                <td>Nama Mentor</td>
                <td>Jabatan</td>
                <td>Email</td>
                <td>No HP</td>
                <td>Pendidikan</td>
                <td>Unit</td>
                <td>Cabang</td>
                <td>Rayon</td>
            </tr>
            </thead>
            
            
            <?php
					
            $dataPerPage = 20;
			$pageNo = 1;
				if(isset($_REQUEST['pageNo']))
				{
					$pageNo = $_REQUEST['pageNo'];
				}
                
				$offset = ($pageNo - 1) * $dataPerPage;
			

                    //SEARCH YA INI
                    $search = '';
                    $search2 = '';
                    $where = "";
                    if(!empty($_POST['txtSearchNama'])){
                        $search = $_POST['txtSearchNama'];
                    }
                    if(!empty($_POST['txtSearchNIP'])){
                        $search2 = $_POST['txtSearchNIP'];
                    }

                    $where = " WHERE NIM != '' ";
                    if($search != ''){
                        $where .= " AND Nama LIKE '%".$search."%' ";
                    }
                    if($search2 != ''){
                        $where .= " AND NIM LIKE '%".$search2."%' ";
                    }

            $qs = "select * FROM msmentor MM INNER JOIN msunit MU ON MM.UnitID=MU.UnitID INNER JOIN mscabang MC ON MM.CabangID=MC.CabangID INNER JOIN msrayon MR ON MM.RayonID=MR.RayonID ".$where." LIMIT $offset, $dataPerPage";
            $query = mysql_query($qs);
            $no=0;
            while($row = mysql_fetch_array($query)){
                $no++;
            
        ?>
     
            <tr class="active" >
                <td><?php echo $no;?></td>
                <td><?php echo $row['NIM'];?></td>
                <td><?php echo $row['Nama'];?></td>
                <td><?php echo $row['Jabatan'];?></td>
                <td><?php echo $row['Email'];?></td>
                <td><?php echo $row['HP'];?></td>
                <td><?php echo $row['Pendidikan'];?></td>
                <td><?php echo $row['UnitName'];?></td>
                <td><?php echo $row['NamaCabang'];?></td>
                <td><?php echo $row['NamaRayon'];?></td>
              
                
                <?php if($_SESSION['role'] == "adminpusat"){ ?>
                <td>
                   
                    <a href="javascript:lightbox(null, 'view/edit-detail-mentor.php?&NIM=<?php echo $row['NIM'];?>')">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-pencil"></i>
                            </button>
                    </a>
                </td>
                <td>
                    <a id="delete_detail" onclick="return OnSubmit()" href="controller/doDelete-detail-mentor.php?NIM=<?php echo $row['NIM'];?>">
                            <button type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-trash-o"></i>
                            </button>
                    </a>
                </td>
                
                <?php }?>
            </tr>
        <?php }?>
            
         
          
        </table>
        
         <div style="text-align: center;">
                <?php
    		$count = 0;
				$queryProduct = "select count(*) 'count'  FROM msmentor";
				$rsProduct = mysql_query($queryProduct);
				
				$rowProduct = mysql_fetch_array($rsProduct);
				
				$count = $rowProduct[0];
				
				$totalPage = ceil($count/$dataPerPage);

				?>
				
                </div>
           
          
           
           
        </div>
        
        <div class="row">
					<div class="col-xs-12" align="center">
						
						<ul class="pagination">
						<?php				
							for($i = 1; $i <= $totalPage; $i++)
							{
								if($i == $pageNo)
								{
									echo"<li class=\"active\"><a>$i</a></li>";
								}
								else
								{?>
									<li><a align='center' href='home.php?menu=MENTOR&pageNo=<?php echo $i;?>'><?php echo $i;?></a></li>
								<?php	}
								
								echo "&nbsp;";
							}
						?>
						</ul>
					</div>
				</div>
        </div>
        </div>
       