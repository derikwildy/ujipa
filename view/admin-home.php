<?php include('connect.php');

?>

<link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>

    
    <div class="row">
        <div class="col-lg-11">
            <div class="col-md-4">
                <h1>Geo View Visitor</h1>
                <script type="text/javascript" src="http://jg.revolvermaps.com/2/6.js?i=6kxrs2gh4vj&amp;m=7&amp;s=320&amp;c=e63100&amp;cr1=ffffff&amp;f=arial&amp;l=0&amp;bv=90&amp;lx=-420&amp;ly=420&amp;hi=20&amp;he=7&amp;hc=a8ddff&amp;rs=80" async="async"></script>
            </div>
        
            <div class="col-md-8">
                <div class="col-md-6">
                    <h1>New Update Class</h1>
                </div>
                <div class="col-md-6 pull" id="download-admin">
                    
                    <button class="dropdown btn btn-primary btn-sm" >
                     <a href="#" class="dropdown-toggle" id="download-admin" data-toggle="dropdown"><i class="fa fa-laptop"></i> Download Format Excel  <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="download.php?name=insert-detail-itc.xls">Detail Kelas</a></li>
                        <li><a href="download.php?name=insert-peserta.xls">Detail Peserta</a></li>
                        <li><a href="download.php?name=insert-mentor.xls">Detail Mentor</a></li>
                      </ul>
                    </button>
                
                </div>
                
                
                
                
            <ul class="">
                                  
            <div class="col-md-12">
                <h3 id="headercol">EE I</h3>
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education I' AND StatusKelas='Active' ORDER BY Tanggal DESC LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                       
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
            
            
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education I' AND StatusKelas='Cancelled' ORDER BY Tanggal DESC LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                        
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
                
                
            </div>
                
            <div class="col-md-12">
                <h3 id="headercol">EE II</h3>
                <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education II' AND StatusKelas='Active' LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                       
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
            
            
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education II' AND StatusKelas='Cancelled' LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                        
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
            </div>  
                 
             
        <div class="col-md-12">
                <h3 id="headercol">EE III</h3>
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education III' AND StatusKelas='Active' LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                       
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
            
            
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Tahap</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE NamaKelas='Executive Education III' AND StatusKelas='Cancelled' LIMIT 2";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                  
                <tbody>
                    <tr class="active">
                        
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                            ?></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div>
            
            
        </div>  
                 
           
          </ul> 
            </div>
        </div>
    </div>


   

