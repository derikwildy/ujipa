<?php

?>


        
 	<div class="row">
    
    <div class="col-lg-12">
        <div class="col-md-12">
        <ol class="breadcrumb">
        
            <div class="col-md-6" id="headercol">
                
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  SORT REPORT EXECUTIVE EDUCATION II</li>
            </div>
        
            <div class="col-md-6" >
                <form class="form-horizontal" name="input_data" action="home.php?menu=REPORTEE2FULL" method="post">
                <div class="form-group col-md-11 pull-right">
                          
                          
                           <tr >
                                
                           
                            <input type="text" name="txtSearchNIP" class="form-control col-md-5" id="txtSearchNIP" placeholder=" NIP">
                            <input type="text" name="txtSearchNama" class="form-control col-md-6" id="txtSearchNama" placeholder="Nama Peserta">
                            <button class="btn btn-info"><span class="fa fa-search" id="searchsp"></span></button>
                            </tr>
                          
                  </div>
                </form>
            </div>
        </ol>
    </div>

        <div class="table-responsive col-md-12">
        <table class="table table-striped table-hover" >
            <thead id="headercol">
            <tr >
                <td>No</td>
                <td>NIP</td>
                <td>Nama Peserta</td>
                <td>Judul</td>
                <td>Nama Mentor</td>
                <td>Tahap</td>
                <td>Tanggal</td>
                <td>Status Kehadiran</td>
            </tr>
            </thead>
            <?php 
                $search = '';
                $search2 = '';
                $where = "";
                if(!empty($_POST['txtSearchNama'])){
                    $search = $_POST['txtSearchNama'];
                }
                if(!empty($_POST['txtSearchNIP'])){
                    $search2 = $_POST['txtSearchNIP'];
                }

                $where = " AND MP.NIP != '' ";
                if($search != ''){
                    $where .= " AND MP.Nama LIKE '%".$search."%' ";
                }
                if($search2 != ''){
                    $where .= " AND MP.NIP LIKE '%".$search2."%' ";
                }
            

            $qs = "select MP.NIP AS NIP, MP.Nama AS NamaPeserta, DK.Judul AS Judul, MM.Nama AS NamaMentor, MT.TahapNama AS Tahap, HK.Tanggal AS Tanggal, DK.StatusLulus AS StatusLulus FROM trheaderkelas HK INNER JOIN trdetailkelas DK ON HK.HeaderKelasID=DK.HeaderKelasID INNER JOIN mskelas MK ON HK.KelasID=MK.KelasID INNER JOIN mstahap MT ON HK.TahapID=MT.TahapID INNER JOIN mspegawai MP ON DK.NIP=MP.NIP INNER JOIN msmentor MM ON DK.NIM=MM.NIM WHERE MK.NamaKelas = 'Executive Education II' ".$where." ORDER BY MP.Nama ASC, MT.TahapID ASC, HK.Tanggal ASC";
            $query = mysql_query($qs);
            $no = 0;
            while($row = mysql_fetch_array($query)){
                $no++;
            
        ?>
     
            <tr class="active" >
                <td><?php echo $no;?></td>
                <td><?php echo $row['NIP'];?></td>
                <td><?php echo $row['NamaPeserta'];?></td>
                <td><?php echo $row['Judul'];?></td>
                <td><?php echo $row['NamaMentor'];?></td>
                <td><?php echo $row['Tahap'];?></td>
                <td><?php echo date("d M Y", strtotime($row['Tanggal']));?></td>
                <td><?php echo $row['StatusLulus'];?></td>
                
            </tr>
        <?php }?>
            
         
          
        </table>
        
         
        </div>
        </div>
        
        </div>