<?php
include('connect.php');
//$id = $_REQUEST['id'];
$no = $_REQUEST['txtno']; 
//$newmenu = $_REQUEST['newmenu'];

?>
<html>
<head>
<title></title>
<link href="asset/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="asset/css/animate.css" type="text/css" rel="stylesheet">
<link href="asset/css/custom.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="images/wpid-pln-logo.png" />
</link>

<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">


</head>

<body>

<div class="conpopup">
    
    <div class="col-md-6">
                <ol class="breadcrumb">
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i> Form Input Peserta</li>
                </ol>
            </div>
    
	<div class="row" id="headercol">
    	<div class="col-md-12">
        
            <form class="form-horizontal" name="input_data" id="input_data" action="controller/doInsert-detail-peserta.php" method="post">
              <fieldset>
              
              
              <input type="hidden" name="no" class="form-control" id="no" value="<?php echo $_POST['txtno'];?>"/>
               
              
        <?php
            for($i=1;$i<=$no;$i++){
        ?>
        <div class="row" id="inputpeserta">         
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputNIP" class="col-lg-4 control-label">NIP</label>
                  <div class="col-lg-8">
                    <input type="text" name="nip<?php echo $i?>" class="form-control" id="nip<?php echo $i?>" placeholder="NIP">
                  </div>
                </div>
            </div>   
            
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputPeserta" class="col-lg-4 control-label">NAMA PESERTA</label>
                  <div class="col-lg-8">
                    <input type="text" name="nama<?php echo $i?>" class="form-control" id="nama<?php echo $i?>" placeholder="Nama Peserta">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputJabatan" class="col-lg-4 control-label">JABATAN</label>
                  <div class="col-lg-8">
                    <input type="text" name="jabatan<?php echo $i?>" class="form-control" id="jabatan<?php echo $i?>" placeholder="Jabatan">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputTanggalLahir" class="col-lg-4 control-label">TANGGAL LAHIR</label>
                  <div class="col-lg-8">
                    <input type="text" name="tanggallahir<?php echo $i?>" class="form-control" id="tanggallahir<?php echo $i?>" placeholder="Tanggal Lahir">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputEmail" class="col-lg-4 control-label">EMAIL</label>
                  <div class="col-lg-8">
                    <input type="text" name="email<?php echo $i?>" class="form-control" id="email<?php echo $i?>" placeholder="Email">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputNoHP" class="col-lg-4 control-label">NO HP</label>
                  <div class="col-lg-8">
                    <input type="text" name="nohp<?php echo $i?>" class="form-control" id="nohp<?php echo $i?>" placeholder="Nomor HP">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputPendidikan" class="col-lg-4 control-label">PENDIDIKAN</label>
                  <div class="col-lg-8">
                    <input type="text" name="pendidikan<?php echo $i?>" class="form-control" id="pendidikan<?php echo $i?>" placeholder="Pendidikan">
                  </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                  <label for="selectNIP" class="col-lg-4 control-label">KELAS</label>
                  <div class="col-lg-8">

                    <select class="form-control" id="kelas<?php echo $i?>" name="kelas<?php echo $i?>">   
                        <?php 
                                $qs = "SELECT * FROM mskelas";
                                $query = mysql_query($qs);
        
                     
                            //PENGULANGAN DATA NAMA INSTRUKTUR
                    
                            while($row = mysql_fetch_array($query)){
                        ?>
                           <option value="<?php echo $row['KelasID'] ?>"><?php echo $row['NamaKelas']?></option>
                        <?php 
                            }
                        ?>
                      </select> 

                  </div>
                </div>
            </div>  
            
             <div class="col-md-6">
                <div class="form-group">
                  <label for="selectNIP" class="col-lg-4 control-label">ANGKATAN</label>
                  <div class="col-lg-8">
                    <select class="form-control" id="angkatan<?php echo $i?>" name="angkatan<?php echo $i?>" style="width:190px">   
                        <option value="Baru">Baru</option>
                        <option value="Lama">Lama</option>
                    </select> 
                  </div>
                </div>
            </div>
                      
               <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputCabang" class="col-lg-4 control-label">NAMA RAYON</label>
                  <div class="col-lg-8">
                      
                      <select class="form-control" id="rayon<?php echo $i?>" name="rayon<?php echo $i?>" style="width:190px">   
                        <?php 
                                $qs4 = "SELECT * FROM msrayon";
                                $query4 = mysql_query($qs4);
        
                     
                            //PENGULANGAN DATA RAYON
                    
                            while($row4 = mysql_fetch_array($query4)){
                        ?>
                           <option value="<?php echo $row4['RayonID'] ?>"><?php echo $row4['NamaRayon']?></option>
                        <?php 
                            }
                        ?>
                      </select> 
                  </div>
                </div>
            </div>
            
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputUnit" class="col-lg-4 control-label">NAMA UNIT</label>
                  <div class="col-lg-8">
                      
                      <select class="form-control" id="unit<?php echo $i?>" name="unit<?php echo $i?>" style="width:190px">   
                        <?php 
                                $qs2 = "SELECT * FROM msunit";
                                $query2 = mysql_query($qs2);
        
                     
                            //PENGULANGAN DATA UNIT
                    
                            while($row2 = mysql_fetch_array($query2)){
                        ?>
                           <option value="<?php echo $row2['UnitID'] ?>"><?php echo $row2['UnitName']?></option>
                        <?php 
                            }
                        ?>
                      </select> 
                  </div>
                </div>
            </div>
            
            
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="inputCabang" class="col-lg-4 control-label">NAMA CABANG</label>
                  <div class="col-lg-8">
                      
                      <select class="form-control" id="cabang<?php echo $i?>" name="cabang<?php echo $i?>" style="width:190px">   
                        <?php 
                                $qs3 = "SELECT * FROM mscabang";
                                $query3 = mysql_query($qs3);
        
                     
                            //PENGULANGAN DATA CABANG
                    
                            while($row3 = mysql_fetch_array($query3)){
                        ?>
                           <option value="<?php echo $row3['CabangID'] ?>"><?php echo $row3['NamaCabang']?></option>
                        <?php 
                            }
                        ?>
                      </select> 
                  </div>
                </div>
            </div>
            
           
            
           
            
            
            
            
         </div>
                  
        

                  
          <?php }?>    
                  
                  
                            
                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    
                    <button type="submit" class="btn btn-primary" name="submit" onclick="return CekValidation()">Submit</button>
                  </div>
                </div>
              </fieldset>
            </form> 
    	</div>
	</div>
</div>


</body>







</html>