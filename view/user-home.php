<?php include('connect.php');
?>

<link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>

<div class="conhome">
    
    <div class="col-md-4">
                <ol class="breadcrumb">
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  My Progress</li>
                </ol>
        </div>
    
        <?php 
            if(isset($_REQUEST['msg'])){
        ?>
            <div class="col-md-8">
                  <div class="alert alert-warning alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div id="notifadmin"><?php echo $_REQUEST['msg'];?></div>
                </div>
            </div>
        <?php }
        else if(isset($_SESSION['err']))
        {
    ?>
            <div class="col-md-8">
                  <div class="alert alert-warning alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div id="notifadmin"><?php echo $_SESSION['err'];?></div>
                </div>
            </div>
    <?php }?>
    
    <div class="row" id="pbbottom">
        <div class="col-lg-12">
           

<div class="checkout-wrap">
  <ul class="checkout-bar">
        <?php 
            $kelas = mysql_query("select * from mspegawai where NIP='".$_SESSION['nip']."'");
            $kelas1 = mysql_fetch_array($kelas);
            
            $qs1 = "select * from mspegawai MP INNER JOIN mskelas MK ON MP.KelasID=MK.KelasID INNER JOIN detailprogress DP ON MP.NIP=DP.NIP WHERE MP.NIP = '".$_SESSION['nip']."' AND MP.KelasID = '".$kelas1['KelasID']."' ORDER BY progressid DESC ";
            
            $query1 = mysql_query($qs1);
            $row = mysql_fetch_array($query1);
        ?>
      
            <li class="visited first">Login</li>
      <!-- UNTUK ICT -->
        <?php if($row['ICT']=='1') { ?>
            <li class="previous visited">ICT/Pembekalan  PA</li>
        <?php } else if($row['ICT']=='0') { ?>
            <li class="active">ICT/Pembekalan PA</li>
        <?php } ?>
      
      <!-- UNTUK F2F1 -->
        <?php if($row['F2F1']=='1') {  ?>
            <li class="previous visited">F2F I</li>
        <?php } else if($row['F2F1']=='0') { ?>
            <li class="active">F2F 1</li>
        <?php } ?>
      
      <!-- UNTUK F2F2 -->
        <?php if($row['F2F2']=='1') {  ?>
            <li class="previous visited">F2F 2</li>
        <?php } else if($row['F2F2']=='0') { ?>
            <li class="active">F2F 2</li>
        <?php } ?>
      
       <!-- UNTUK UJI PA-->
        <?php if($row['UJIPA']=='1') {  ?>
            <li class="previous visited">UJI PA</li>
        <?php } else if($row['UJIPA']=='0') { ?>
            <li class="active">UJI PA</li>
        <?php } ?>
      
  </ul>
</div>
        </div>
    </div>
    
    

   <div class="col-md-4">
    <ol class="breadcrumb">
             <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  My Info</li>
        </ol>
    </div>
    
    <div class="col-lg-9"> 
        
  <div class="table-responsive">
              <table class="table table-hover table-striped tablesorter">
                <thead id="headercol">
                  <tr>
                    <th> <i class="fa fa-sort"></i></th>
                    <th>Tahap</th>
                    <th>Kelas</th>
                    <th>Tanggal</th>
                    <th>Status Kelas</th>
                    <th>Status Kehadiran</th>
                  </tr>
                </thead>
                <?php 
                    $qs = "select * from trheaderkelas HK inner join trdetailkelas DK on HK.HeaderKelasID = DK.HeaderKelasID inner join mstahap T on HK.TahapID = T.TahapID
    INNER JOIN mskelas K on HK.KelasID = K.KelasID inner join mspegawai P on DK.NIP=P.NIP where P.NIP = '".$_SESSION['nip']."' 
    order by HK.KelasID, T.TahapID ,cast(date_format(str_to_date(Tanggal,'%d-%M-%Y'),'%Y-%m-%d') as date) ASC ";
                    $query = mysql_query($qs);
                    while($row = mysql_fetch_array($query)){
                ?>
                <tbody>
                    <tr class="active">
                            <td></td>
                            <td><?php echo $row['TahapNama'];?></td>
                            <td><?php echo $row['NamaKelas'];?></td>
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                            <td><?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                
                            ?></td>
                            <td>
                            <?php 
                         //ICT       
                        if($row['TahapID']!="4"){
                                if($row['StatusLulus'] == "Gagal") echo "Tidak Hadir";
                                else if($row['StatusLulus'] == "Lulus") echo "Hadir";
                        }
                        else{
                                if($row['StatusLulus'] == "Gagal") echo "Gagal";
                                else if($row['StatusLulus'] == "Lulus") echo "Lulus";
                        }      
                                
                            ?></td>
                            
                    </tr>
                </tbody>
                <?php }?>
              </table>
            </div> 

       </div>

 
    
   