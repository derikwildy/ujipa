
        <div class="row">
          <div class="col-lg-12">
              <div class="row">
            <div class="col-md-6">
                <ol class="breadcrumb">
                  <li class="active"  id="activecrumb"><i class="fa fa-dashboard" id="txtheaderadmin"></i>  Executive Education I : <b>UJI PA</b></li>
                </ol>
            </div>
            <?php
                if(isset($_REQUEST['msg'])){
            ?>
            <div class="col-md-6">
                  <div class="alert alert-warning alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <div id="notifadmin"><?php echo $_REQUEST['msg'];?></div>
                </div>
            </div>
            <?php }?>
                  </div>
              
              <div class="row">
        
                   <div class="col-lg-2 pull-right" id="insertbtnm">
                  <?php if($_SESSION['role'] == "admin"){ ?>
                       <a href="javascript:lightbox2(null, 'view/insert-class.php?newmenu=EE1UJIPA&tahap=4&kelas=1')">
                                    <button class="pull-right btn btn-primary btn-sm right-mg"  id="fontadm"><i class="fa fa-plus"></i> </button>
                        </a>
                        
                    <?php }?>
                  
             </div>
                  
              </div>
            <div class="row">
            <div class="table-responsive col-md-12">
              <table class="table table-hover table-striped tablesorter" >
                <thead id="headercol">
                  <tr>
                    <th>No </th>
                    <th>Status</th>
                    <th>Kelas</th>
                    <th>Jumlah</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
               
                  
                   <?php
					
            $dataPerPage = 20;
			$pageNo = 1;
				if(isset($_REQUEST['pageNo']))
				{
					$pageNo = $_REQUEST['pageNo'];
				}
                
				$offset = ($pageNo - 1) * $dataPerPage;
			

                    $qs = "select * from trheaderkelas HK INNER JOIN MsKelas MK ON 
                    HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE TahapNama='Uji PA' 
                    AND NamaKelas='Executive Education I' order by cast(date_format(str_to_date(Tanggal,'%d-%M-%Y'),'%Y-%m-%d') as date)  desc LIMIT $offset, $dataPerPage";
                    $query = mysql_query($qs);
                    $no = 0;
                    while($row = mysql_fetch_array($query)){ 
                        $no++;
                ?>
                <tbody>
                    <tr class="active">
                            <td><?php echo $no;?></td>
                            <td>
                            <?php 
                                if($row['StatusKelas']=="Active"){
                                    echo '<p class="txtstatusactive"> <i class="fa fa-check-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else if($row['StatusKelas']=="Available"){
                                    echo '<p class="txtstatusavail"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                else{
                                    echo '<p class="txtstatusdis"> <i class="fa fa-circle"></i>  '.$row['StatusKelas'].'<p>';
                                }
                                
                            ?></td>
                            <td><?php echo $row['NamaKelas'];?></td>
                            <td><?php echo $row['Jumlah']; ?></td> 
                            <td><?php echo date('d F Y', strtotime($row['Tanggal']));?></td>
                            <td>
                                <a href="javascript:lightbox(null, 'view/detail-ee1-ujipa.php?id=<?php echo $row['HeaderKelasID'];?>&kelas=1');">
                                <!--<a href="view/detail-ee1-f2f1.php?id=<?php echo $row['HeaderKelasID'];?>">-->
                                    <button type="button" id="fontadm" class="btn btn-primary btn-sm">
                                         <i class="fa fa-search"></i>   View
                                       
                                    </button>
                                </a>
                            </td>
                            
                            
                            <td>
                                <?php if($_SESSION['role']=="admin") { ?>
                                <a href="javascript:lightbox2(null, 'view/edit-class.php?id=<?php echo $row['HeaderKelasID'];?>&newmenu=EE2UJIPA&tahap=4&kelas=1');">
                                
                                    <button type="button" id="fontadm" class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil"></i>   Edit
                                      
                                    </button>
                                </a>
                                <?php }?>
                            </td>
                            
                            <td>
                                <?php if($_SESSION['role']=="admin") {?>
                                <a href="javascript:lightbox(null, 'view/updateKelulusan.php?id=<?php echo $row['HeaderKelasID'];?>&newmenu=EE1UJIPA&tahap=4&kelas=1');">
                                <!--<a href="view/detail-ee1-f2f1.php?id=<?php echo $row['HeaderKelasID'];?>">-->
                                    <button type="button" id="fontadm" class="btn btn-primary btn-sm">
                                         <i class="fa fa-check-square"></i>    Kelulusan
                                       
                                    </button>
                                </a>
                                <?php } ?>
                            </td>
                    </tr>
                </tbody>
                <?php }?>
              </table>
                
                 
                
               <div style="text-align: center;">
                <?php
    		$count = 0;
				$queryProduct = "select count(*) from trheaderkelas HK INNER JOIN MsKelas MK ON 
                    HK.KelasID=MK.KelasID INNER JOIN MsTahap MT ON HK.TahapID=MT.TahapID WHERE TahapNama='Uji PA' 
                    AND NamaKelas='Executive Education I'";
				$rsProduct = mysql_query($queryProduct);
				
				$rowProduct = mysql_fetch_array($rsProduct);
				
				$count = $rowProduct[0];
				
				$totalPage = ceil($count/$dataPerPage);

				?>
				<div class="row">
					<div class="col-xs-12" align="center">
						
						<ul class="pagination">
						<?php				
							for($i = 1; $i <= $totalPage; $i++)
							{
								if($i == $pageNo)
								{
									echo"<li class=\"active\"><a>$i</a></li>";
								}
								else
								{?>
									<li><a align='center' href='home.php?menu=EE1UJIPA&kelas=1&pageNo=<?php echo $i;?>'><?php echo $i;?></a></li>
								<?php	}
								
								echo "&nbsp;";
							}
						?>
						</ul>
					</div>
				</div>
                </div>
            </div>
           </div> 
          </div>
            
         
        </div><!-- /.row -->