<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         
           <div class="row">
               <div class="col-lg-2">
                     <a class="navbar-brand" href="home.php?menu=user"><img src="assets/img/PLN.jpg" class="logopln pull-left" >
                    </a>
                </div>
                
            </div>
        </div>
    
        <div class="navbar-header1">
            <div class="col-lg-6">
                 <a class="navbar-brand" href="home.php?menu=user"><img src="assets/img/logo.png" class="logo pull-right" >
                   </a>
                </div>
           
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            
          <ul class="nav navbar-nav side-nav">
                                   <li> <a  href="home.php?menu=admin">
                            <img id="center" src="assets/img/admin.png" width="130px" height="130px" alt="...">
                        </a>
              
               <div class="media-body">
                    <?php if($_SESSION['role']=="admin"){?>
                        <h3 class="media-heading" id="txt">ADMIN UDIKLAT</h3>  </div>
                    <?php } ?>
                    <?php if($_SESSION['role']=="adminpusat"){?>
                        <h3 class="media-heading" id="txt">ADMIN PNT</h3>  </div>
                    <?php } ?>
              
              </li>
              <li><a href="home.php?menu=admin"><i class="fa fa-home"></i> Home</a></li>
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-laptop"></i> Executive Education I <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="home.php?menu=EE1ICT&kelas=1">ICT / Pembekalan PA</a></li>
                <li><a href="home.php?menu=EE1F2F1&kelas=1">Face To Face I</a></li>
                <li><a href="home.php?menu=EE1F2F2&kelas=1">Face To Face II</a></li>
                <li><a href="home.php?menu=EE1UJIPA&kelas=1">Uji PA</a></li>
                <li><a href="home.php?menu=REPORTEE1&kelas=1">Report EE I</a></li> 
              </ul>
            </li>
              
               <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-laptop"></i> Executive Education II <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="home.php?menu=EE2ICT&kelas=2">ICT / Pembekalan PA</a></li>
                <li><a href="home.php?menu=EE2F2F1&kelas=2">Face To Face I</a></li>
                <li><a href="home.php?menu=EE2F2F2&kelas=2">Face To Face II</a></li>
                <li><a href="home.php?menu=EE2UJIPA&kelas=2">Uji PA</a></li>
                <li><a href="home.php?menu=REPORTEE2&kelas=2">Report EE II</a></li> 
              </ul>
            </li>
              
               <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-laptop"></i> Executive Education III <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="home.php?menu=EE3ICT&kelas=3">ICT / Pembekalan PA</a></li>
                <li><a href="home.php?menu=EE3F2F1&kelas=3">Face To Face I</a></li>
                <li><a href="home.php?menu=EE3F2F2&kelas=3">Face To Face II</a></li>
                <li><a href="home.php?menu=EE3UJIPA&kelas=3">Uji PA</a></li>
                <li><a href="home.php?menu=REPORTEE3&kelas=3">Report EE III</a></li> 
              </ul>
            </li>
               <li><a href="home.php?menu=PESERTA"><i class="fa fa-users "></i>  Participant Registration</a></li>
                <li><a href="home.php?menu=MENTOR"><i class="fa fa-user "></i>  Mentor Registration</a></li>
                <li><a href="home.php?menu=REPORTALL"><i class="fa fa-copy"></i>  Report</a></li>
              <li><a href="controller/dologout.php"><i class="fa fa-power-off "></i>  Sign out</a></li>
          </ul>
            
          <ul class="nav navbar-nav navbar-right navbar-user">
           
          </ul>
          
        </div><!-- /.navbar-collapse -->
      </nav>