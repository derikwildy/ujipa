-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 09 Sep 2014 pada 04.24
-- Versi Server: 5.5.32
-- Versi PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `ujipa`
--
CREATE DATABASE IF NOT EXISTS `ujipa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ujipa`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detailprogress`
--

CREATE TABLE IF NOT EXISTS `detailprogress` (
  `progressID` int(11) NOT NULL AUTO_INCREMENT,
  `NIP` varchar(100) NOT NULL,
  `ICT` varchar(100) NOT NULL,
  `F2F1` varchar(100) NOT NULL,
  `F2F2` varchar(100) NOT NULL,
  `UJIPA` varchar(100) NOT NULL,
  `KelasID` varchar(20) NOT NULL,
  PRIMARY KEY (`progressID`,`NIP`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data untuk tabel `detailprogress`
--

INSERT INTO `detailprogress` (`progressID`, `NIP`, `ICT`, `F2F1`, `F2F2`, `UJIPA`, `KelasID`) VALUES
(1, '1234567890', '1', '0', '0', '0', '1'),
(2, '1234567890', '0', '0', '0', '0', '3'),
(3, '1111', '1', '0', '0', '0', '2'),
(4, '121212', '1', '1', '0', '1', '1'),
(5, '131313', '1', '1', '0', '0', '1'),
(6, '1414', '0', '0', '0', '0', '1'),
(7, '21321312', '0', '0', '0', '0', '3'),
(8, '1717', '0', '0', '0', '0', '2'),
(9, '1616', '0', '0', '0', '0', '1'),
(10, '1515', '0', '0', '0', '0', '3'),
(11, '5985006Z', '0', '0', '0', '0', '1'),
(12, '6085009J', '1', '1', '0', '0', '1'),
(13, '6184014J', '0', '0', '0', '0', '1'),
(14, '6392014Z', '1', '0', '0', '0', '1'),
(15, '6593054Z', '1', '0', '0', '0', '1'),
(16, '6585077P2B', '1', '0', '0', '0', '1'),
(17, '6692066Z', '1', '0', '0', '0', '1'),
(18, '6795103P', '1', '0', '0', '0', '1'),
(19, '6993213Z', '1', '1', '1', '1', '1'),
(20, '6895069', '0', '0', '0', '0', '1'),
(21, '6895069', '0', '0', '0', '0', '2'),
(22, '6384052E', '0', '0', '0', '0', '2'),
(23, '6995126R', '0', '0', '0', '0', '1'),
(24, '6692132J', '0', '0', '0', '0', '1'),
(25, '6893181Z', '0', '0', '0', '0', '1'),
(26, '6991055J', '0', '0', '0', '0', '1'),
(27, '6794018P', '0', '0', '0', '0', '1'),
(28, '6594009L', '0', '0', '0', '0', '2'),
(29, '7095062P', '0', '0', '0', '0', '2'),
(30, '6693091Z', '0', '0', '0', '0', '1'),
(31, '321321', '0', '0', '0', '0', '1'),
(32, 'DSA', '0', '0', '0', '0', '1'),
(33, 'DSADS', '0', '0', '0', '0', '1'),
(34, 'JHGJHG', '0', '0', '0', '0', '1'),
(35, 'DSA', '0', '0', '0', '0', '1'),
(36, 'JHGHG', '0', '0', '0', '0', '1'),
(37, 'FDSSFD', '0', '0', '0', '0', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mskelas`
--

CREATE TABLE IF NOT EXISTS `mskelas` (
  `KelasID` varchar(20) NOT NULL,
  `NamaKelas` varchar(100) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  PRIMARY KEY (`KelasID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mskelas`
--

INSERT INTO `mskelas` (`KelasID`, `NamaKelas`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`) VALUES
('1', 'Executive Education I', '', '', '', ''),
('2', 'Executive Education II', '', '', '', ''),
('3', 'Executive Education III', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `msmentor`
--

CREATE TABLE IF NOT EXISTS `msmentor` (
  `NIM` varchar(100) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Unit` text NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  PRIMARY KEY (`NIM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `msmentor`
--

INSERT INTO `msmentor` (`NIM`, `Nama`, `Unit`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`) VALUES
('00', 'NASRI SEBAYANG', '', '', '', '', ''),
('000000001', 'SETYO ANGGORO DEWO', 'PT PLN (PERSERO) KANTOR PUSAT', '', '', '', ''),
('00001', 'EDDY D ERNINGPRAJA', 'PT PLN (PERSERO) KANTOR PUSAT', '', '', '', ''),
('0001', 'NGURAH ADNYANA', 'PT PLN (PERSERO) KANTOR PUSAT', '', '', '', ''),
('001', 'SETIO ANGGORO DEWO', 'PT PLN (PERSERO) KANTOR PUSAT', '', '', '', ''),
('5783001P', 'NASRI SEBAYANG', 'PT PLN (PERSERO) KANTOR PUSAT', '', '', '', ''),
('5893124P', 'SIBADU', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mspegawai`
--

CREATE TABLE IF NOT EXISTS `mspegawai` (
  `NIP` varchar(100) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `UnitID` varchar(100) NOT NULL,
  `Angkatan` varchar(100) NOT NULL,
  `KelasID` int(11) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  `LastLogin` varchar(100) NOT NULL,
  PRIMARY KEY (`NIP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mspegawai`
--

INSERT INTO `mspegawai` (`NIP`, `Nama`, `Password`, `UnitID`, `Angkatan`, `KelasID`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`, `LastLogin`) VALUES
('5985006Z', 'HERWIANDONO', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', '2014-09-01 15:29:49'),
('6085009J', 'JOHNNY PALMA', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', '2014-09-08 15:27:59'),
('6184014J', 'AGUS BACHTIAR AZIZ', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', ''),
('6384052E', 'BOYKE DJONES SONDAKH', '6384052E', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6392014Z', 'ACHMAD ZUDJADJ', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', ''),
('6585077P2B', 'DEDI RUSPENDI', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', ''),
('6593054Z', 'HENRISON ADVENTIUS LUMBANRAJA', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', '2014-09-02 13:39:23'),
('6594009L', 'IKETUT GEDE AGUS SUTOPO', '6594009L', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6692066Z', 'JEFRI ROSIADI', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', ''),
('6692132J', 'DWI WIBIHANDOKO', '6692132J', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6794018P', 'HIDMAD ERHANSYAH', '6794018P', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6795103P', 'AJI SUTRISNO', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', '2014-09-01 13:40:53'),
('6893181Z', 'ERICSON SARAGI SADABUTAR', '6893181Z', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6895069P', 'ALLAND ASWOLANI', '6895069P', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6991055J', 'HAENI SUSETYA', '6991055J', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('6993213Z', 'EKO WARSITO', '1212', 'U001', 'Baru', 1, '', '', 'adminpusat', '01-September-2014', '2014-09-02 16:26:25'),
('6995126R', 'DEFIAR ANIS', '6995126R', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('7095062P', 'KUNTO NUGROHO', '7095062P', 'U001', 'Baru', 2, 'adminpusat', '08-September-2014', '', '', ''),
('admin', 'admin', 'admin', 'admin', 'Baru', 0, '', '', '', '', '2014-09-08 14:30:51'),
('adminpusat', 'adminpusat', 'adminpusat', 'adminpusat', 'Baru', 0, '', '', '', '', '2014-09-08 14:42:12'),
('FDSSFD', 'FDSFDS', '1212', 'U003', 'Baru', 1, 'adminpusat', '08-September-2014', 'adminpusat', '08-September-2014', '2014-09-08 15:21:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mstahap`
--

CREATE TABLE IF NOT EXISTS `mstahap` (
  `TahapID` varchar(20) NOT NULL,
  `TahapNama` varchar(100) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  PRIMARY KEY (`TahapID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mstahap`
--

INSERT INTO `mstahap` (`TahapID`, `TahapNama`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`) VALUES
('1', 'ICT', '', '', '', ''),
('2', 'Face to Face 1', '', '', '', ''),
('3', 'Face to Face 2', '', '', '', ''),
('4', 'Uji PA', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `msunit`
--

CREATE TABLE IF NOT EXISTS `msunit` (
  `UnitID` varchar(10) NOT NULL,
  `UnitName` varchar(200) NOT NULL,
  PRIMARY KEY (`UnitID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `msunit`
--

INSERT INTO `msunit` (`UnitID`, `UnitName`) VALUES
('U001', 'ACCENTURE'),
('U002', 'AKLI'),
('U003', 'AKLI DPP'),
('U004', 'AKLINA'),
('U005', 'ANUGRAH TEKNIK UTAMA'),
('U006', 'BADAN TENAGA NUKLIR NASIONAL'),
('U007', 'BMKG MEDAN'),
('U008', 'BPPT'),
('U009', 'CHEVRON PACIFIC INDONESIA'),
('U010', 'CV ADI TEHNIK'),
('U011', 'CV AGUNG ELEKTRIK'),
('U012', 'CV ARAHMA TEKNIK'),
('U013', 'CV ARTA PRIMA PERKASA'),
('U014', 'CV BANUA SEJATI'),
('U015', 'CV DIANPRATAMA TEKNIK'),
('U016', 'CV MULYA ABADI'),
('U017', 'CV PENDOWO'),
('U018', 'CV SUMBERDAYA VANI'),
('U019', 'CV. BT. KILAT'),
('U020', 'CV. BT. KILAT Total'),
('U021', 'Departemen Pemerintahan Kota Medan'),
('U022', 'DEPNAKERTRANS'),
('U023', 'DINAS PENDIDIKAN DAN PENGAJARAN JAYA WIJAYA'),
('U024', 'DINAS PENDIDIKAN KABUPATEN JAYAPURA'),
('U025', 'DITJEN EBTKE KEMENTRIAN ESDM'),
('U026', 'DPC AKLI  KALSEL-TENG'),
('U027', 'DPC AKLI KOTA PALEMBANG'),
('U028', 'DPC AKLI NTB'),
('U029', 'DPC AKLI NTT'),
('U030', 'DPD AKLINDO SUMSEL'),
('U031', 'GAE'),
('U032', 'General Electric'),
('U033', 'HITACHI'),
('U034', 'INSTITUT TEKNOLOGI BANDUNG (ITB)'),
('U035', 'INSTITUT TEKNOLOGI SEPULUH NOPEMBER SURABAYA'),
('U036', 'KOMIPO PEMBANGKITAN JAWA DAN BALI'),
('U037', 'KONSUIL'),
('U038', 'KOPERASI MEKAR MELATI'),
('U039', 'KOPKAR LISTRIK MUARA BELITI'),
('U040', 'KOPKAR LISTRIK RANTING PENDOPO'),
('U041', 'KOPKAR LISTRIK SEMINUNG'),
('U042', 'KOPKAR PLN RANTING KOBA'),
('U043', 'KOPKAR PLN RANTING MENTOK'),
('U044', 'KOPKAR PLN UDIKLAT SEMARANG'),
('U045', 'KYUSHU ELECTRIC'),
('U046', 'Mitsui Co.Ltd'),
('U047', 'POLDA SUMATRA UTARA'),
('U048', 'POLITEKNIK NEGERI BALI'),
('U049', 'POLITEKNIK NEGERI JAKARTA'),
('U050', 'POLITEKNIK NEGERI MALANG'),
('U051', 'POLITEKNIK NEGERI MEDAN'),
('U052', 'POLITEKNIK NEGERI PADANG'),
('U053', 'POLITEKNIK NEGERI SEMARANG'),
('U054', 'POLITEKNIK NEGERI SRIWIJAYA PALEMBANG'),
('U055', 'POLITEKNIK NEGERI UJUNG PANDANG'),
('U056', 'PROFESIONAL'),
('U057', 'PT ALSSA CORPORINDO'),
('U058', 'PT ARLISCO'),
('U059', 'PT ARMADIAN PERKASA'),
('U060', 'PT ARUTMIN INDONESIA'),
('U061', 'PT ATIGA SURYA'),
('U062', 'PT BANK MANDIRI (PERSERO) Tbk.'),
('U063', 'PT BINABUANA SEWAKA'),
('U064', 'PT BIRU SAKTI'),
('U065', 'PT BUKIT ASAM (Persero) TBK'),
('U066', 'PT BUKIT TINORING RAYA'),
('U067', 'PT CIREBON POWER SERVICES'),
('U068', 'PT COGINDO DAYA BERSAMA'),
('U069', 'PT DWITAMA PUTRA PRATAMA'),
('U070', 'PT ERICSSON INDONESIA'),
('U071', 'PT FAKONMAS'),
('U072', 'PT FREEPORT INDONESIA PAPUA TIMIKA'),
('U073', 'PT GORDA PRIMA'),
('U074', 'PT HANIL JAYA STEEL'),
('U075', 'PT HANSON ENERGY (Persero)'),
('U076', 'PT INDONESIA ASAHAN ALUMUNIUM'),
('U077', 'PT INDONESIA COMNETS PLUS'),
('U078', 'PT INDONESIA POWER'),
('U079', 'PT INDOSMELT'),
('U080', 'PT INDRA KENCANA NUSANTARA'),
('U081', 'PT INTER WORLD STEEL'),
('U082', 'PT ITOCHU'),
('U083', 'PT ITRON'),
('U084', 'PT JASNIKOM GEMANUSA'),
('U085', 'PT JATI BINANGUN JAYA'),
('U086', 'PT JONATHAN AGUNG BERSAUDARA'),
('U087', 'PT KASIH INDUSTRI INDONESIA'),
('U088', 'PT KIDECO JAYA AGUNG'),
('U089', 'PT KRAKATAU POSCOPOWER'),
('U090', 'PT LASER JAYA MANDIRI'),
('U091', 'PT LSI'),
('U092', 'PT MARDIKA SARANA ENGGINEERING'),
('U093', 'PT MEDCO E & P INDONESIA MEPI'),
('U094', 'PT MOBILKOM TELEKOMINDO'),
('U095', 'PT MULYA SEJAHTERA'),
('U096', 'PT PANCARAN MAS SEMESTA'),
('U097', 'PT PELAYANAN LISTRIK NASIONAL BATAM'),
('U098', 'PT PELAYANAN LISTRIK NASIONAL TARAKAN'),
('U099', 'PT PELAYARAN BAHTERA ADHIGUNA'),
('U100', 'PT PEMBANGKITAN JAWA BALI'),
('U101', 'PT PEMBANGKITAN JAWA BALI SERVICES'),
('U102', 'PT PENGEMBANG LISTRIK NASIONAL GEOTHERMAL'),
('U103', 'PT PERTAMINA'),
('U104', 'PT PERTAMINA GEOTHERMAL ENERGY'),
('U105', 'PT PETROKIMIA GRESIK'),
('U106', 'PT PLN (PERSERO) DISTRIBUSI BALI'),
('U107', 'PT PLN (PERSERO) DISTRIBUSI JAKARTA RAYA DAN TANGERANG'),
('U108', 'PT PLN (PERSERO) DISTRIBUSI JAWA BARAT DAN BANTEN'),
('U109', 'PT PLN (PERSERO) DISTRIBUSI JAWA TENGAH DAN D.I. YOGYAKARTA'),
('U110', 'PT PLN (PERSERO) DISTRIBUSI JAWA TIMUR'),
('U111', 'PT PLN (PERSERO) JASA MANAJEMEN KONSTRUKSI'),
('U112', 'PT PLN (PERSERO) JASA SERTIFIKASI'),
('U114', 'PT PLN (PERSERO) P3B JAWA BALI'),
('U115', 'PT PLN (PERSERO) P3B SUMATERA'),
('U116', 'PT PLN (PERSERO) PEMBANGKITAN CILEGON'),
('U117', 'PT PLN (PERSERO) PEMBANGKITAN INDRAMAYU'),
('U118', 'PT PLN (PERSERO) PEMBANGKITAN LONTAR'),
('U119', 'PT PLN (PERSERO) PEMBANGKITAN MUARA TAWAR'),
('U121', 'PT PLN (PERSERO) PEMBANGKITAN SUMATERA BAGIAN UTARA'),
('U122', 'PT PLN (PERSERO) PEMBANGKITAN TANJUNG JATI B'),
('U123', 'PT PLN (PERSERO) PIKITRING JAWA, BALI DAN NUSRA'),
('U124', 'PT PLN (PERSERO) PIKITRING SUMATERA DAN ACEH'),
('U125', 'PT PLN (PERSERO) PROYEK INDUK PEMBANGKIT DAN JARINGAN SULAWESI, MALUKU DAN PAPUA'),
('U126', 'PT PLN (PERSERO) PROYEK INDUK PEMBANGKIT DAN JARINGAN SUMATERA UTARA, ACEH DAN RIAU'),
('U127', 'PT PLN (PERSERO) PUSAT ENJINERING KETENAGALISTRIKAN'),
('U128', 'PT PLN (PERSERO) PUSAT PEMELIHARAAN KETENAGALISTRIKAN'),
('U129', 'PT PLN (PERSERO) PUSAT PENDIDIKAN DAN PELATIHAN'),
('U130', 'PT PLN (PERSERO) PUSAT PENELITIAN DAN PENGEMBANGAN'),
('U131', 'PT PLN (PERSERO) TUGAS KARYA'),
('U133', 'PT PLN (PERSERO) UIP JARINGAN SULAWESI, MALUKU DAN PAPUA'),
('U134', 'PT PLN (PERSERO) UIP JARINGAN SUMATERA I'),
('U135', 'PT PLN (PERSERO) UIP JARINGAN SUMATERA II'),
('U136', 'PT PLN (PERSERO) UIP KITRING KALIMANTAN'),
('U137', 'PT PLN (PERSERO) UIP PEMBANGKIT & JARINGAN KALIMANTAN'),
('U138', 'PT PLN (PERSERO) UIP PEMBANGKIT & JARINGAN NUSA TENGGARA'),
('U139', 'PT PLN (PERSERO) UIP PEMBANGKIT HIDRO JAWA BALI'),
('U140', 'PT PLN (PERSERO) UIP PEMBANGKIT SULAWESI, MALUKU DAN PAPUA'),
('U141', 'PT PLN (PERSERO) UIP PEMBANGKIT SUMATERA I'),
('U142', 'PT PLN (PERSERO) UIP PEMBANGKIT SUMATERA II'),
('U143', 'PT PLN (PERSERO) UIP PEMBANGKIT THERMAL JAWA BALI'),
('U144', 'PT PLN (PERSERO) UIP TRANSMISI INTERKONEKSI JAWA-SUMATERA'),
('U146', 'PT PLN (PERSERO) WILAYAH ACEH'),
('U147', 'PT PLN (PERSERO) WILAYAH BANGKA BELITUNG'),
('U148', 'PT PLN (PERSERO) WILAYAH KALIMANTAN BARAT'),
('U149', 'PT PLN (PERSERO) WILAYAH KALIMANTAN SELATAN DAN KALIMANTAN TENGAH'),
('U150', 'PT PLN (PERSERO) WILAYAH KALIMANTAN TIMUR'),
('U151', 'PT PLN (PERSERO) WILAYAH LAMPUNG'),
('U152', 'PT PLN (PERSERO) WILAYAH MALUKU DAN MALUKU UTARA'),
('U153', 'PT PLN (PERSERO) WILAYAH NUSA TENGGARA BARAT'),
('U154', 'PT PLN (PERSERO) WILAYAH NUSA TENGGARA TIMUR'),
('U155', 'PT PLN (PERSERO) WILAYAH PAPUA DAN PAPUA BARAT'),
('U156', 'PT PLN (PERSERO) WILAYAH RIAU DAN KEPULAUAN RIAU'),
('U157', 'PT PLN (PERSERO) WILAYAH SULAWESI SELATAN, SULAWESI TENGGARA DAN SULAWESI BARAT'),
('U158', 'PT PLN (PERSERO) WILAYAH SULAWESI UTARA, SULAWESI TENGAH DAN GORONTALO'),
('U159', 'PT PLN (PERSERO) WILAYAH SUMATERA BARAT'),
('U160', 'PT PLN (PERSERO) WILAYAH SUMATERA SELATAN, JAMBI DAN BENGKULU'),
('U161', 'PT PLN (PERSERO) WILAYAH SUMATERA UTARA'),
('U162', 'PT PLN BATUBARA'),
('U163', 'PT PRIMA LAYANAN NASIONAL ENJINERING'),
('U164', 'PT PRIMA PERSADA NUSANTARA'),
('U165', 'PT PUNCAK JAYA POWER'),
('U166', 'PT REKADAYA ELEKTRIKA'),
('U167', 'PT SUMBER SEGARA PRIMADAYA'),
('U168', 'PT SURVEYOR INDONESIA (PERSERO)'),
('U169', 'PT TECHNOFO INDONESIA'),
('U170', 'PT Telekomunikasi Indonesia, Tbk'),
('U171', 'PT TRAFO INDONESIA'),
('U172', 'PT TRINUSA'),
('U173', 'PT YASA EXSPANSIA SEJAHTERA'),
('U174', 'PT. ANDAREX AGAPE'),
('U175', 'PT. BAHTERA ADIGUNA'),
('U176', 'PT. BAKARA BUMI ENERGI'),
('U177', 'PT. BALFOUR BEATTY SAKTI INDONESIA'),
('U178', 'PT. BEGAWAN CITRA MANDIRI'),
('U179', 'PT. BORNEO ADELIA PRATAMA'),
('U180', 'PT. CAHAYA CITRA MULIA'),
('U181', 'PT. CAHAYA FAJAR KALTIM'),
('U182', 'PT. CENTRAL OMEGA RESOURCES, TBK'),
('U183', 'PT. DATA ENERGY INFOMEDIA'),
('U184', 'PT. GANENDRA PARAKA SATRIA'),
('U185', 'PT. GEMILANG'),
('U186', 'PT. GEO DIPA ENERGI'),
('U187', 'PT. GREEN WIJAYA'),
('U188', 'PT. HANKOOK TIRE INDONESIA'),
('U189', 'PT. HUIDA KARYA MANDIRI'),
('U190', 'PT. INDO CITRA MANDIRI'),
('U191', 'PT. INDO GLOBAL INTERNATIONAL'),
('U192', 'PT. JALAMAS BERKATAMA'),
('U193', 'PT. KALTIM PRIMA COAL'),
('U194', 'PT. KARISMA UTAMA'),
('U195', 'PT. KARLIS MANDIRI BATRA RAYA'),
('U196', 'PT. KARYA ALAS MANDIRI'),
('U197', 'PT. KARYA ENERGI'),
('U198', 'PT. KRAKATAU DAYA LISTRIK'),
('U199', 'PT. KWJ'),
('U200', 'PT. MAHKOTA SARINEMBAH SEJAHTERA'),
('U201', 'PT. MAJU BERSAMA MANDIRI'),
('U202', 'PT. Manunggal Pratama'),
('U203', 'PT. MITRA INSAN UTAMA'),
('U204', 'PT. NEWMONT NUSA TENGGARA'),
('U205', 'PT. NUSANTARA INDO ENERGI'),
('U206', 'PT. PERSADA LAMPUNG NUSANTARA'),
('U207', 'PT. PUSAKA JAYA PALU POWER'),
('U208', 'PT. QUALITA SEJAHTERA'),
('U209', 'PT. RAZZA PRIMA TRAFO'),
('U210', 'PT. REGINA LINTAS PERSADA'),
('U211', 'PT. SCHNEIDER INDONESIA'),
('U212', 'PT. SENTRA'),
('U213', 'PT. SMARTMETER'),
('U214', 'PT. TEGUH CAHAYA ADILLA'),
('U215', 'PT. UNINDO'),
('U216', 'PT. YUNIKA FASADA'),
('U217', 'PT. YUVISAL'),
('U218', 'PT.SARANA ELEKTRIK INFORMATIKA'),
('U219', 'PURNAKARYA'),
('U220', 'QA/QC CONSULTANT (LIMCA)'),
('U221', 'SATUAN PENGAWAS INTERN'),
('U222', 'SEN Engineering Company'),
('U223', 'SMK  NEGERI 1 SENTANI'),
('U224', 'SMK  NEGERI 2 PAREPARE'),
('U225', 'SMK 1 KEDUNGWUNI'),
('U226', 'SMK 1 PUNDONG BANTUL'),
('U227', 'SMK 1 SUMATERA BARAT'),
('U228', 'SMK 2 DESEMBER PELAIHARI'),
('U229', 'SMK 2 MEI BANDAR LAMPUNG'),
('U230', 'SMK 45 Kota Bima'),
('U231', 'SMK AKP GALANG'),
('U232', 'SMK AL-ISLAH PALANGKA RAYA'),
('U233', 'SMK ALKHAI RAAT MANADO'),
('U234', 'SMK AMSIR 1 PAREPARE'),
('U235', 'SMK ANTASARI'),
('U236', 'SMK BATUR JAYA I CEPER KLATEN'),
('U237', 'SMK BHAKTI BANGSA BANJARBARU'),
('U238', 'SMK BHAKTI PRAJA JEPARA'),
('U239', 'SMK BHAKTI UTAMA BANDAR LAMPUNG'),
('U240', 'SMK BHINEKA BANDAR LAMPUNG'),
('U241', 'SMK Bina Bangsa, Mataram'),
('U242', 'SMK BINA KARYA LARANTUKA FLORES TIMUR'),
('U243', 'SMK BINA KUSUMA RUTENG'),
('U244', 'SMK BINA PATRIA 1 SUKOHARJO'),
('U245', 'SMK BINA PUTRA'),
('U246', 'SMK BINA UTAMA'),
('U247', 'SMK BINA WIYATA KARANGMALANG SRAGEN'),
('U248', 'SMK BLK BANDAR LAMPUNG'),
('U249', 'SMK BOEDI OETOMO 2 GANDRUNGMANGU'),
('U250', 'SMK BUDHI DARMA INDRAPURA'),
('U251', 'SMK BUDI KARYA NATAR LAMPUNG'),
('U252', 'SMK BUDI UTOMO'),
('U253', 'SMK BUDI UTOMO 2 WAY JEPARA LAMPUNG'),
('U254', 'SMK CITRA BORNEO'),
('U255', 'SMK COKROAMINOTO KOTAMOBAGU'),
('U256', 'SMK COKROAMINOTO PANDAK BANTUL'),
('U257', 'SMK DARUSSALAM MARTAPURA'),
('U258', 'SMK DH PEPABRI BULUKUMBA'),
('U259', 'SMK DHARMA PALA PANJANG LAMPUNG'),
('U260', 'SMK DHUAFA PADANG'),
('U261', 'SMK Dr. SOETOMO CILACAP'),
('U262', 'SMK DWI SEJAHTERA PEKANBARU'),
('U263', 'SMK GANESHA 1 SEKAMPUNG LAMPUNG'),
('U264', 'SMK GANESHA METRO'),
('U265', 'SMK HANG TUAH BATAM'),
('U266', 'SMK HARAPAN GENERASI MANADO'),
('U267', 'SMK HASANAH PEKANBARU'),
('U268', 'SMK ILE LEWOTOLOK LEWOLEBA'),
('U269', 'SMK ISLAM INAYAH'),
('U270', 'SMK KANSAI PEKANBARU'),
('U271', 'SMK KARSA MULYA PALANGKA RAYA'),
('U272', 'SMK KARTIKA IV BALIKPAPAN'),
('U273', 'SMK KARYA BERINGIN LHOKSEUMAWE'),
('U274', 'SMK KARYA DHARMA 1 ABUNG SELATAN LAMPUNG'),
('U275', 'SMK KARYA KUPANG'),
('U276', 'SMK KARYA PADANG PANJANG'),
('U277', 'SMK KARYA TEKNIK SOPPENG'),
('U278', 'SMK KELAUTAN MENTARI'),
('U279', 'SMK KOSGORO 1 PADANG'),
('U280', 'SMK KOSGORO SINGKAWANG'),
('U281', 'SMK KRISTEN 1 KLATEN'),
('U282', 'SMK KRISTEN 1 TOMOHON'),
('U283', 'SMK KRISTEN 2 KOTA KUPANG'),
('U284', 'SMK KRISTEN 2 KUPANG'),
('U285', 'SMK KRISTEN GETSEMANI MANADO'),
('U286', 'SMK LEONARDO KLATEN'),
('U287', 'SMK MAARIF 1 PIYUNGAN BANTUL'),
('U288', 'SMK MALESUNG MANADO'),
('U289', 'SMK MASMUR PEKANBARU'),
('U290', 'SMK MUDA KREATIF BARABAI'),
('U291', 'SMK MUHAMMADIYAH  PEKALONGAN'),
('U292', 'SMK MUHAMMADIYAH 1 PEKANBARU'),
('U293', 'SMK MUHAMMADIYAH 1 TEGAL'),
('U294', 'SMK MUHAMMADIYAH 3 SAMARINDA'),
('U295', 'SMK MUHAMMADIYAH AMBON'),
('U296', 'SMK MUHAMMADIYAH BATAM'),
('U297', 'SMK MUHAMMADIYAH BUNGORO PANGKEP'),
('U298', 'SMK MUHAMMADIYAH MAJENANG'),
('U299', 'SMK MUHAMMADIYAH MUNGKID'),
('U300', 'SMK MUHAMMADIYAH SALATIGA'),
('U301', 'SMK MUHAMMADIYAH SINTANG'),
('U302', 'SMK Muhammadiyah, Mataram'),
('U303', 'SMK MUHAMMADYAH 1 TERBANGGI BESAR LAMPUNG'),
('U304', 'SMK MUHAMMADYAH 2 KALIREJO LAMPUNG'),
('U305', 'SMK MUHAMMADYAH BUKITTINGGI'),
('U306', 'SMK MUHAMMADYAH METRO LAMPUNG'),
('U307', 'SMK MUHAMMADYAH PADANG'),
('U308', 'SMK MUTIARA 2 NATAR LAMPUNG'),
('U309', 'SMK NASIONAL BATAM'),
('U310', 'SMK NEGERI  2 BANJARBARU'),
('U311', 'SMK NEGERI 1 ADIWERNA'),
('U312', 'SMK NEGERI 1 BALIKPAPAN'),
('U313', 'SMK NEGERI 1 BANGKINANG'),
('U314', 'SMK NEGERI 1 BANGKO BAGAN SIAPI-API'),
('U315', 'SMK NEGERI 1 BATAM'),
('U316', 'SMK NEGERI 1 BAULA'),
('U317', 'SMK NEGERI 1 BAULA KOLAKA'),
('U318', 'SMK NEGERI 1 BELIMBING'),
('U319', 'SMK NEGERI 1 BENER MERIAH'),
('U320', 'SMK NEGERI 1 BIAK'),
('U321', 'SMK NEGERI 1 BINTAN'),
('U322', 'SMK NEGERI 1 BIREUEN'),
('U323', 'SMK NEGERI 1 BONTOMANAI SELAYAR'),
('U324', 'SMK NEGERI 1 BOVEN DIGOEL'),
('U325', 'SMK NEGERI 1 BUKIT KEMUNING LAMPUNG'),
('U326', 'SMK NEGERI 1 CURUP KOTA'),
('U327', 'SMK NEGERI 1 DLINGO BANTUL'),
('U328', 'SMK NEGERI 1 ENTIKONG'),
('U329', 'SMK NEGERI 1 GALESONG SELATAN KABUPATEN TAKALAR'),
('U330', 'SMK NEGERI 1 GAMBUT'),
('U331', 'SMK NEGERI 1 JEUMPA - BIREUEN'),
('U332', 'SMK NEGERI 1 KAHAYAN HILIR'),
('U333', 'SMK NEGERI 1 KAINUI SERUI'),
('U334', 'SMK NEGERI 1 KARIMUN'),
('U335', 'SMK NEGERI 1 KAROSSA'),
('U336', 'SMK NEGERI 1 KAUR'),
('U337', 'SMK NEGERI 1 KEEROM'),
('U338', 'SMK NEGERI 1 KEFAMENANU'),
('U339', 'SMK NEGERI 1 KENDAWANGAN'),
('U340', 'SMK NEGERI 1 KEPAHIANG'),
('U341', 'SMK NEGERI 1 KOTA JANTHO'),
('U342', 'SMK NEGERI 1 LABUAN KABUPATEN DONGGALA'),
('U343', 'SMK NEGERI 1 LARANTUKA'),
('U344', 'SMK NEGERI 1 LHOKSUKON'),
('U345', 'SMK NEGERI 1 LILIRIAJA'),
('U346', 'SMK NEGERI 1 MANDAU'),
('U347', 'SMK NEGERI 1 MAUMERE'),
('U348', 'SMK NEGERI 1 MERBAU'),
('U349', 'SMK NEGERI 1 MIMIKA'),
('U350', 'SMK NEGERI 1 MINASATENE KABUPATEN PANGKEP'),
('U351', 'SMK NEGERI 1 MUKOMUKO'),
('U352', 'SMK NEGERI 1 NABIRE'),
('U353', 'SMK NEGERI 1 NGABANG'),
('U354', 'SMK NEGERI 1 OBAA'),
('U355', 'SMK NEGERI 1 PADANG'),
('U356', 'SMK NEGERI 1 PALOH'),
('U357', 'SMK NEGERI 1 PANGKALAN KERINCI'),
('U358', 'SMK NEGERI 1 PARIAMAN'),
('U359', 'SMK NEGERI 1 PARINDU'),
('U360', 'SMK NEGERI 1 PARINGIN'),
('U361', 'SMK NEGERI 1 PERCUT SEI TUAN'),
('U362', 'SMK NEGERI 1 PLERET BANTUL'),
('U363', 'SMK NEGERI 1 POMALAA'),
('U364', 'SMK NEGERI 1 PORTIBI'),
('U365', 'SMK NEGERI 1 RAMBAH'),
('U366', 'SMK NEGERI 1 RANGAS KABUPATEN MAMUJU'),
('U367', 'SMK NEGERI 1 RATAHAN'),
('U368', 'SMK NEGERI 1 RENGAT BARAT'),
('U369', 'SMK NEGERI 1 SAPARUA'),
('U370', 'SMK NEGERI 1 SEDAYU BANTUL'),
('U371', 'SMK NEGERI 1 SEKADAU'),
('U372', 'SMK NEGERI 1 SEMARANG'),
('U373', 'SMK NEGERI 1 SEMPARUK'),
('U374', 'SMK NEGERI 1 SENGAH TEMILA'),
('U375', 'SMK NEGERI 1 SENGKANG'),
('U376', 'SMK NEGERI 1 SEPUTIH AGUNG LAMPUNG'),
('U377', 'SMK NEGERI 1 SEPUTIH SURABAYA LAMPUNG'),
('U378', 'SMK NEGERI 1 SESEAN'),
('U379', 'SMK NEGERI 1 SIAK'),
('U380', 'SMK NEGERI 1 SIMPANG EMPAT'),
('U381', 'SMK NEGERI 1 SIMPANG ULIM'),
('U382', 'SMK NEGERI 1 SINGKAWANG'),
('U383', 'SMK NEGERI 1 SINGKEP'),
('U384', 'SMK NEGERI 1 SINTANG'),
('U385', 'SMK NEGERI 1 SOE'),
('U386', 'SMK NEGERI 1 SUKAMARA'),
('U387', 'SMK Negeri 1 SUKAMARA KALTENG'),
('U388', 'SMK NEGERI 1 SUMBER JAYA LAMPUNG'),
('U389', 'SMK NEGERI 1 SUNGAI PINANG'),
('U390', 'SMK NEGERI 1 SUTERA'),
('U391', 'SMK NEGERI 1 TAHAH PINOH'),
('U392', 'SMK NEGERI 1 TAKISUNG'),
('U393', 'SMK NEGERI 1 TANAH PUTIH'),
('U394', 'SMK NEGERI 1 TANDUN'),
('U395', 'SMK NEGERI 1 TANJUNG RAYA'),
('U396', 'SMK NEGERI 1 TAPIN SELATAN'),
('U397', 'SMK NEGERI 1 TELUK KUANTAN'),
('U398', 'SMK NEGERI 1 TILATANG KAMANG'),
('U399', 'SMK NEGERI 1 TIMIKA'),
('U400', 'SMK NEGERI 1 TOMOHON'),
('U401', 'SMK NEGERI 1 TOULUAAN MINAHASA TENGGARA'),
('U402', 'SMK NEGERI 1 TUMPAAN'),
('U403', 'SMK NEGERI 1 URAM JAYA'),
('U404', 'SMK NEGERI 1 WAY TENONG LAMPUNG'),
('U405', 'SMK NEGERI 1 WEWEWA BARAT'),
('U406', 'SMK NEGERI 2 ARGAMAKMUR'),
('U407', 'SMK NEGERI 2 BANDA ACEH'),
('U408', 'SMK NEGERI 2 BANDAR LAMPUNG'),
('U409', 'SMK NEGERI 2 BARABAI'),
('U410', 'SMK NEGERI 2 BARRU'),
('U411', 'SMK NEGERI 2 BAUBAU'),
('U412', 'SMK NEGERI 2 BENER MERIAH'),
('U413', 'SMK NEGERI 2 BIAK'),
('U414', 'SMK NEGERI 2 BITUNG'),
('U415', 'SMK NEGERI 2 BULIK KABUPATEN LAMANDAU'),
('U416', 'SMK NEGERI 2 BUNGORO'),
('U417', 'SMK NEGERI 2 CILACAP'),
('U418', 'SMK NEGERI 2 CURUP TIMUR'),
('U419', 'SMK NEGERI 2 DOLOK SANGGUL HUMBANG HASUNDUTAN'),
('U420', 'SMK NEGERI 2 DUMAI'),
('U421', 'SMK NEGERI 2 ENDE'),
('U422', 'SMK NEGERI 2 JAYAPURA'),
('U423', 'SMK NEGERI 2 JENEPONTO'),
('U424', 'SMK NEGERI 2 KALIANDA LAMPUNG'),
('U425', 'SMK NEGERI 2 KANDANGAN'),
('U426', 'SMK NEGERI 2 KARANG BARU'),
('U427', 'SMK NEGERI 2 KEBUMEN'),
('U428', 'SMK NEGERI 2 KENDAL'),
('U429', 'SMK NEGERI 2 KENDARI'),
('U430', 'SMK NEGERI 2 KEPAHIANG'),
('U431', 'SMK NEGERI 2 KETAPANG'),
('U432', 'SMK NEGERI 2 KLATEN'),
('U433', 'SMK NEGERI 2 KOTA BENGKULU'),
('U434', 'SMK NEGERI 2 KOTA KUPANG'),
('U435', 'SMK NEGERI 2 KUPANG'),
('U436', 'SMK NEGERI 2 LANGSA'),
('U437', 'SMK NEGERI 2 LUBUK BASUNG'),
('U438', 'SMK NEGERI 2 LUWUK'),
('U439', 'SMK NEGERI 2 MANADO'),
('U440', 'SMK NEGERI 2 MANOKWARI'),
('U441', 'SMK NEGERI 2 MARABAHAN'),
('U442', 'SMK NEGERI 2 MEULABOH'),
('U443', 'SMK NEGERI 2 MUKOMUKO'),
('U444', 'SMK NEGERI 2 NABIRE'),
('U445', 'SMK NEGERI 2 PAINAN'),
('U446', 'SMK NEGERI 2 PALOPO'),
('U447', 'SMK NEGERI 2 PANGKALAN BUN'),
('U448', 'SMK NEGERI 2 PAYAKUMBUH'),
('U449', 'SMK NEGERI 2 PEKANBARU'),
('U450', 'SMK NEGERI 2 PELAIHARI'),
('U451', 'SMK NEGERI 2 PEUREULAK'),
('U452', 'SMK NEGERI 2 PONTIANAK'),
('U453', 'SMK NEGERI 2 PURWODADI'),
('U454', 'SMK NEGERI 2 PURWOKERTO'),
('U455', 'SMK NEGERI 2 RAHA'),
('U456', 'SMK NEGERI 2 SAMARINDA'),
('U457', 'SMK NEGERI 2 SAWAHLUNTO'),
('U458', 'SMK NEGERI 2 SIDENRENG'),
('U459', 'SMK NEGERI 2 SIGLI'),
('U460', 'SMK NEGERI 2 SINABANG'),
('U461', 'SMK NEGERI 2 SOLOK'),
('U462', 'SMK NEGERI 2 SRAGEN'),
('U463', 'SMK NEGERI 2 SURAKARTA'),
('U464', 'SMK NEGERI 2 TAKENGON ACEH TENGAH'),
('U465', 'SMK NEGERI 2 TANSEL'),
('U466', 'SMK NEGERI 2 TEBING TINGGI'),
('U467', 'SMK NEGERI 2 TEMBILAHAN'),
('U468', 'SMK NEGERI 2 TERBANGGI BESAR LAMPUNG'),
('U469', 'SMK NEGERI 2 WAINGAPU'),
('U470', 'SMK NEGERI 2 WATAMPONE'),
('U471', 'SMK NEGERI 2 WATANSOPPENG'),
('U472', 'SMK NEGERI 2 WONOSOBO'),
('U473', 'SMK NEGERI 3 AMBON'),
('U474', 'SMK NEGERI 3 BANTAENG'),
('U475', 'SMK NEGERI 3 BATAM'),
('U476', 'SMK NEGERI 3 DUMAI'),
('U477', 'SMK NEGERI 3 GORONTALO'),
('U478', 'SMK NEGERI 3 JAYAPURA'),
('U479', 'SMK NEGERI 3 KALABAHI'),
('U480', 'SMK NEGERI 3 KAUR'),
('U481', 'SMK NEGERI 3 KOTABUMI LAMPUNG'),
('U482', 'SMK NEGERI 3 MERAUKE'),
('U483', 'SMK NEGERI 3 PALU'),
('U484', 'SMK NEGERI 3 PARIAMAN'),
('U485', 'SMK NEGERI 3 PINRANG'),
('U486', 'SMK NEGERI 3 SELUMA'),
('U487', 'SMK NEGERI 3 SEMARANG'),
('U488', 'SMK NEGERI 3 SIGLI'),
('U489', 'SMK NEGERI 3 SORONG'),
('U490', 'SMK NEGERI 3 TAHUNA KABUPATEN SANGIHE'),
('U491', 'SMK NEGERI 3 TAKENGON'),
('U492', 'SMK NEGERI 3 TANJUNG PINANG'),
('U493', 'SMK NEGERI 3 TERBANGGI BESAR LAMPUNG'),
('U494', 'SMK NEGERI 3 TONDANO'),
('U495', 'SMK NEGERI 4 AMBON'),
('U496', 'SMK NEGERI 4 JAYAPURA'),
('U497', 'SMK NEGERI 4 PINRANG'),
('U498', 'SMK NEGERI 4 PONTIANAK'),
('U499', 'SMK NEGERI 4 SERAM BARAT'),
('U500', 'SMK NEGERI 4 SORONG'),
('U501', 'SMK NEGERI 4 TALAUD'),
('U502', 'SMK NEGERI 5 BALIKPAPAN'),
('U503', 'SMK NEGERI 5 BANJARMASIN'),
('U504', 'SMK NEGERI 5 BATAM'),
('U505', 'SMK NEGERI 5 BENGKULU SELATAN'),
('U506', 'SMK NEGERI 5 KOTA KUPANG'),
('U507', 'SMK NEGERI 5 KUPANG'),
('U508', 'SMK NEGERI 5 LHOKSEUMAWE'),
('U509', 'SMK NEGERI 5 MAJENE'),
('U510', 'SMK NEGERI 5 PADANG'),
('U511', 'SMK NEGERI 5 PEKANBARU'),
('U512', 'SMK NEGERI 5 SOLOK SELATAN'),
('U513', 'SMK NEGERI 5 SURAKARTA'),
('U514', 'SMK NEGERI 5 WAINGAPU SUMBA TIMUR'),
('U515', 'SMK NEGERI 6 BATAM'),
('U516', 'SMK NEGERI 6 JAYAPURA'),
('U517', 'SMK NEGERI 7 SEMARANG'),
('U518', 'SMK NEGERI 9 JAYAPURA'),
('U519', 'SMK NEGERI BERTARAF INTERNASIONAL SUMATERA UTARA'),
('U520', 'SMK NEGERI ENREKANG'),
('U521', 'SMK NEGERI I BULAKAMBA'),
('U522', 'SMK NEGERI KOTA JAYAPURA'),
('U523', 'SMK NEGERI KUNDUR'),
('U524', 'SMK Negeri Nusawungu'),
('U525', 'SMK NEGERI PENERBANGAN ACEH'),
('U526', 'SMK NEGERI SABANG'),
('U527', 'SMK NEGERI SUKAHARJO LAMPUNG'),
('U528', 'SMK NU MAARIF KUDUS'),
('U529', 'SMK NURUL BARQI SEMARANG'),
('U530', 'SMK NUSANTARA 1 COMAL'),
('U531', 'SMK PAB 1 HELVETIA'),
('U532', 'SMK PANGERAN ANTASARI BALIKPAPAN'),
('U533', 'SMK PAYUNG NEGERI SIAK'),
('U534', 'SMK PELITA NUSANTARA 2 SEMARANG'),
('U535', 'SMK PEMDA LABUHAN BATU'),
('U536', 'SMK PEMDA RANTAUPRAPAT'),
('U537', 'SMK PERKAPALAN HANGTUAH'),
('U538', 'SMK PETRA NABIRE'),
('U539', 'SMK PETRA TIMIKA'),
('U540', 'SMK PGRI 2 KEDONDONG LAMPUNG'),
('U541', 'SMK PGRI 2 TERBANGGI BESAR LAMPUNG'),
('U542', 'SMK PGRI PONTIANAK'),
('U543', 'SMK PGRI WATANSOPPENG'),
('U544', 'SMK PUTRA ANDA BINJAI'),
('U545', 'SMK RAJA HAJI TANJUNG PINANG'),
('U546', 'SMK SANJAYA BAJAWA'),
('U547', 'SMK SANTU ANTONIUS MERAUKE'),
('U548', 'SMK SARASWATI SALATIGA'),
('U549', 'SMK SETIA BUDI BALIKPAPAN'),
('U550', 'SMK ST. YOSEF NENUK'),
('U551', 'SMK SUBUR INSANI'),
('U552', 'SMK SUKOWATI SRAGEN'),
('U553', 'SMK SULTAN SY. ABDURRAHMAN'),
('U554', 'SMK SULTAN SYARIF KASIM'),
('U555', 'SMK SWADHIPA NATAR LAMPUNG'),
('U556', 'SMK SYUHADA TEKNOLOGI BANJARMASIN'),
('U557', 'SMK TAMAN KARYA MADYA BANDAR LAMPUNG'),
('U558', 'SMK TARUNA PERSADA DUMAI'),
('U559', 'SMK TEKNOLOGI BALAM KAB.ROKAN HILIR'),
('U560', 'SMK TUNAS HARAPAN PATI'),
('U561', 'SMK WISUDA KARYA'),
('U562', 'SMK Wongsorejo Gombong-Kebumen'),
('U563', 'SMK YAMATU TUALANG'),
('U564', 'SMK YAPESLI WAMENA'),
('U565', 'SMK YASHAKI NABIRE'),
('U566', 'SMK YDB LUBUK ALUNG'),
('U567', 'SMK YPK 1 BIAK'),
('U568', 'SMK YPK 2 BIAK'),
('U569', 'SMK YPK BANJARBARU'),
('U570', 'SMK YPK KOTARAJA JAYAPURA'),
('U571', 'SMK YPK SERUI'),
('U572', 'SMK YPL LIRIK'),
('U573', 'SMK YPP PURWOREJO'),
('U574', 'SMK YPT BANJARMASIN'),
('U575', 'SMK YPT PANGKALAN BRANDAN'),
('U576', 'SMK YPT PRINGSEWU LAMPUNG'),
('U577', 'SMK YPT Purworejo'),
('U578', 'SMK YSO NINABUA WAMENA'),
('U579', 'SMK Yusuf Abdussatar'),
('U580', 'SMKN 1 Alas, Sumbawa'),
('U581', 'SMKN 1 BALIGE'),
('U582', 'SMKN 1 BATIPUH'),
('U583', 'SMKN 1 Bayan, Lombok Utara'),
('U584', 'SMKN 1 Beur, Sumbawa'),
('U585', 'SMKN 1 BONJOL'),
('U586', 'SMKN 1 BUKITTINGGI'),
('U587', 'SMKN 1 BUNGURAN TIMUR'),
('U588', 'SMKN 1 Gerung, Lombok Barat'),
('U589', 'SMKN 1 GUGUK, 50 KOTA'),
('U590', 'SMKN 1 Gunungsari, Lombok Barat'),
('U591', 'SMKN 1 Kopang, Lombok Tengah'),
('U592', 'SMKN 1 KOTO XI TARUSAN'),
('U593', 'SMKN 1 KUTALIMBARU'),
('U594', 'SMKN 1 Labuapi, Lombok Barat'),
('U595', 'SMKN 1 LEMBAH MELINTANG'),
('U596', 'SMKN 1 Lingsar, Lombok Barat'),
('U597', 'SMKN 1 LINTAU BUO'),
('U598', 'SMKN 1 LOBALAIN'),
('U599', 'SMKN 1 LUBUK PAKAM'),
('U600', 'SMKN 1 Maluk, Sumbawa Barat'),
('U601', 'SMKN 1 Manggelewa, Dompu'),
('U602', 'SMKN 1 MEDAN'),
('U603', 'SMKN 1 Plampang, Sumbawa'),
('U604', 'SMKN 1 Pringgabaya, Lombok Timur'),
('U605', 'SMKN 1 SEI KANAN'),
('U606', 'SMKN 1 Selong, Lombok Timur'),
('U607', 'SMKN 1 Seteluk, Sumbawa Barat'),
('U608', 'SMKN 1 SIRANDORUNG'),
('U609', 'SMKN 1 STABAT'),
('U610', 'SMKN 1 Taliwang, Sumbawa Barat'),
('U611', 'SMKN 1 Tanjung, Lombok Utara'),
('U612', 'SMKN 1 Woja, Dompu'),
('U613', 'SMKN 2 Bima'),
('U614', 'SMKN 2 DOLOKSANGGUL'),
('U615', 'SMKN 2 Gerung, Lombok Barat'),
('U616', 'SMKN 2 Kuripan, Lombok Barat'),
('U617', 'SMKN 2 MEDAN'),
('U618', 'SMKN 2 PEMATANG SIANATAR'),
('U619', 'SMKN 2 Praya, Lombok Tengah'),
('U620', 'SMKN 2 SIATAS BARITA'),
('U621', 'SMKN 2 Sumbawa Besar'),
('U622', 'SMKN 3 Dompu'),
('U623', 'SMKN 3 Mataram'),
('U624', 'SMKN 3 Selong, Lombok Timur'),
('U625', 'SMKN 5 MEDAN'),
('U626', 'SMKN 6 Mataram'),
('U627', 'SMKN 9 Mataram'),
('U628', 'SMKN BI SUMUT'),
('U629', 'SMKN LUMBANJULU'),
('U630', 'SMKs 10 TELEKOMUNIKASI'),
('U631', 'SMKs 8 GRAKARSA'),
('U632', 'SMKS BUDHI DARMA INDRAPURA'),
('U633', 'SMKTR SWASTA SINARHUSNI'),
('U634', 'SMTI PONTIANAK'),
('U635', 'STAR ENERGY GEOTHERMAL'),
('U636', 'STEI ITB'),
('U637', 'STT PLN'),
('U638', 'TIM ZIPUR TNI - AD'),
('U639', 'Universitas dan Politeknik Medan'),
('U640', 'UNIVERSITAS DIPONEGORO SEMARANG'),
('U641', 'UNIVERSITAS GADJAH MADA'),
('U642', 'UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA'),
('U643', 'UNIVERSITAS PANCASILA'),
('U644', 'UNIVERSITAS SUMATERA UTARA'),
('U645', 'YANTEK'),
('U646', 'YAYASAN DANA PENSIUN PLN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trdetailkelas`
--

CREATE TABLE IF NOT EXISTS `trdetailkelas` (
  `HeaderKelasID` varchar(100) NOT NULL,
  `NIP` varchar(100) NOT NULL,
  `Judul` varchar(100) NOT NULL,
  `StatusLulus` varchar(100) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  `NIM` varchar(100) NOT NULL,
  `AngkaAngkatan` varchar(100) NOT NULL,
  PRIMARY KEY (`HeaderKelasID`,`NIP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trdetailkelas`
--

INSERT INTO `trdetailkelas` (`HeaderKelasID`, `NIP`, `Judul`, `StatusLulus`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`, `NIM`, `AngkaAngkatan`) VALUES
('KLS00001', '6392014Z', 'Analisis database', 'Lulus', 'adminpusat', '01-September-2014', 'adminpusat', '02/09/2014', '0001', '17'),
('KLS00001', '6585077P2B', 'Analisis dan perancangan', 'Lulus', 'adminpusat', '01-September-2014', 'adminpusat', '02/09/2014', '00001', '16'),
('KLS00001', '6593054Z', 'Management Abdul', 'Lulus', 'adminpusat', '01-September-2014', 'adminpusat', '02/09/2014', '0001', '16'),
('KLS00001', '6692066Z', 'Analisis dan perancangan', 'Lulus', 'adminpusat', '01-September-2014', 'adminpusat', '02/09/2014', '00', '16'),
('KLS00001', '6795103P', 'Analisis dan perancangan', 'Lulus', 'adminpusat', '01-September-2014', 'adminpusat', '02/09/2014', '001', '16'),
('KLS00001', '6993213Z', 'judi1', 'Lulus', 'adminpusat', '02-September-2014', 'adminpusat', '02/09/2014', '00', '16'),
('KLS00007', '6993213Z', 'judi1', 'Lulus', '6993213Z', '02-September-2014', '', '', '00', '16'),
('KLS00008', '6993213Z', 'judi1', 'Lulus', '6993213Z', '02-September-2014', '', '', '00', '16'),
('KLS00011', '6085009J', 'fdfds', 'Lulus', 'adminpusat', '03-September-2014', 'adminpusat', '03/09/2014', '00', '16'),
('KLS00012', '6085009J', 'Analisis database', '', '6085009J', '03-September-2014', '', '', '5893124P', '17'),
('KLS00012', '6795103P', 'Analisis dan perancangan', '', 'admin', '05-September-2014', '', '', '001', '16'),
('KLS00012', '6993213Z', 'judi1', 'Lulus', '6993213Z', '02-September-2014', '', '', '00', '16'),
('KLS00013', '6384052E', 'JUDUL 2', '', 'adminpusat', '08-September-2014', '', '', '00', '8'),
('KLS00013', '6692132J', 'JUDUL 4', '', 'adminpusat', '08-September-2014', '', '', '0001', '8'),
('KLS00013', '6893181Z', 'JUDUL 5', '', 'adminpusat', '08-September-2014', '', '', '001', '8'),
('KLS00013', '6895069P', 'JUDUL 1', '', 'adminpusat', '08-September-2014', '', '', '00001', '8'),
('KLS00013', '6991055J', 'JUDUL 6', '', 'adminpusat', '08-September-2014', '', '', '5893124P', '8'),
('KLS00013', '6995126R', 'JUDUL 3', '', 'adminpusat', '08-September-2014', '', '', '5783001P', '8');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trdetailpa`
--

CREATE TABLE IF NOT EXISTS `trdetailpa` (
  `HeaderPAID` varchar(100) NOT NULL,
  `KelasID` varchar(20) NOT NULL,
  `Status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trheaderkelas`
--

CREATE TABLE IF NOT EXISTS `trheaderkelas` (
  `HeaderKelasID` varchar(100) NOT NULL,
  `KelasID` varchar(20) NOT NULL,
  `Jumlah` int(11) NOT NULL,
  `Tanggal` varchar(100) NOT NULL,
  `StatusKelas` varchar(100) NOT NULL,
  `TahapID` varchar(20) NOT NULL,
  `CreatedBy` varchar(100) NOT NULL,
  `CreatedDate` varchar(100) NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL,
  `UpdatedDate` varchar(100) NOT NULL,
  PRIMARY KEY (`HeaderKelasID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trheaderkelas`
--

INSERT INTO `trheaderkelas` (`HeaderKelasID`, `KelasID`, `Jumlah`, `Tanggal`, `StatusKelas`, `TahapID`, `CreatedBy`, `CreatedDate`, `UpdatedBy`, `UpdatedDate`) VALUES
('KLS00001', '1', 6, '10-September-2014', 'Active', '1', 'adminpusat', '01-September-2014', '6085009J', '08-September-2014'),
('KLS00002', '1', 0, '01-January-1970', 'Cancelled', '2', '6795103P', '01-January-1970', '6085009J', '08-September-2014'),
('KLS00003', '1', 0, '01-January-1970', 'Cancelled', '2', '6085009J', '01-January-1970', '6085009J', '08-September-2014'),
('KLS00004', '1', 0, '09-October-2014', 'Available', '2', '6085009J', '09-October-2014', '6085009J', '08-September-2014'),
('KLS00005', '1', 0, '19-September-2014', 'Cancelled', '1', 'adminpusat', '01-September-2014', '6085009J', '08-September-2014'),
('KLS00006', '1', 0, '10-October-2014', 'Available', '3', '6085009J', '10-October-2014', '6085009J', '08-September-2014'),
('KLS00007', '1', 2, '12-December-2014', 'Available', '3', '6085009J', '12-December-2014', '6085009J', '08-September-2014'),
('KLS00008', '1', 1, '15-January-2015', 'Available', '4', '6085009J', '15-January-2015', '6085009J', '08-September-2014'),
('KLS00009', '1', 0, '', 'Cancelled', '1', 'adminpusat', '02-September-2014', '6085009J', '08-September-2014'),
('KLS00010', '1', 0, '03-September-2014', 'Cancelled', '1', 'adminpusat', '02-September-2014', '6085009J', '08-September-2014'),
('KLS00011', '1', 1, '26-September-2014', 'Available', '1', 'adminpusat', '02-September-2014', '6085009J', '08-September-2014'),
('KLS00012', '1', 3, '11-December-2014', 'Available', '2', '6993213Z', '02-September-2014', '6085009J', '08-September-2014'),
('KLS00013', '2', 6, '09-September-2014', 'Active', '1', 'adminpusat', '08-September-2014', '6085009J', '08-September-2014');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trheaderpa`
--

CREATE TABLE IF NOT EXISTS `trheaderpa` (
  `HeaderPAID` varchar(100) NOT NULL,
  `NIP` varchar(100) NOT NULL,
  `Angkatan` varchar(100) NOT NULL,
  `JudulPA` varchar(200) NOT NULL,
  PRIMARY KEY (`HeaderPAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
