<?php
include("connect.php");
session_start();

if(!isset($_SESSION['nama'])){
    header("location:index.php?err=Silahkan login terlebih dahulu");
}

if(isset($_REQUEST['menu'])){
    $menu = $_REQUEST['menu'];
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Portal UJI PA</title>

      
      <link href="assets/img/PLN.jpg" rel='icon' type='image/x-icon'/>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="assets/css/style.css" rel="stylesheet">
   
    <link rel="stylesheet" href="assets/fonts/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="assets/css/morris-0.4.3.min.css">
    <script src="assets/js/lightbox.js"></script>
    <link rel="stylesheet" href="assets/css/lightbox.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php 
      if($_SESSION['role']=="admin"|| $_SESSION['role']=="adminpusat"){
        include("sidemenu.php"); 
      }
      else{
        include("sidemenu-user.php");
      }
      ?>

        
        
     <div id="page-wrapper">

        
            <?php
            
            if($_SESSION['role']=="admin" || $_SESSION['role']=="adminpusat")
            {
                if(!isset($menu))
                {
                    //kosong aja
                }
                else if($menu == "")
                {
                    //kosong aja
                }
                else if($menu == "admin")
                {
                    include("view/admin-home.php");
                }
                else if($menu == "EE1ICT"){
                    include("view/admin-ee1-ict.php");
                }
                else if($menu=="EE1F2F1"){
                    include("view/admin-ee1-f2f1.php");
                }
                else if($menu=="EE1F2F2"){
                    include("view/admin-ee1-f2f2.php");
                }
                else if($menu=="EE1UJIPA"){
                    include("view/admin-ee1-ujiPA.php");
                }
                else if($menu == "EE2ICT"){
                    include("view/admin-ee2-ict.php");
                }
                else if($menu=="EE2F2F1"){
                    include("view/admin-ee2-f2f1.php");
                }
                else if($menu=="EE2F2F2"){
                    include("view/admin-ee2-f2f2.php");
                }
                else if($menu=="EE2UJIPA"){
                    include("view/admin-ee2-ujiPA.php");
                }
                 else if($menu == "EE3ICT"){
                    include("view/admin-ee3-ict.php");
                }
                else if($menu=="EE3F2F1"){
                    include("view/admin-ee3-f2f1.php");
                }
                else if($menu=="EE3F2F2"){
                    include("view/admin-ee3-f2f2.php");
                }
                else if($menu=="EE3UJIPA"){
                    include("view/admin-ee3-ujiPA.php");
                }
                else if($menu=="NEWDETAIL"){
                    include("view/insert-detail.php");
                }
                else if($menu=="EDITDETAIL"){
                    include("view/edit-detail.php");
                }
                else if($menu=="PESERTA"){
                    include("view/dataParticipant.php");
                }
                else if($menu=="MENTOR"){
                    include("view/dataMentor.php");
                }
                //ini baru ya
                else if($menu=="TAMBAHPESERTA"){
                    include("view/insert-detail-peserta.php");
                }
                else if($menu=="TAMBAHMENTOR"){
                    include("view/insert-detail-mentor.php");
                }
                else if($menu=="REPORTEE1"){
                    include("view/report-menu-ee1.php");
                }
                else if($menu=="REPORTEE2"){
                    include("view/report-menu-ee2.php");
                }
                else if($menu=="REPORTEE3"){
                    include("view/report-menu-ee3.php");
                }
                 else if($menu=="REPORTEE1FULL"){
                    include("view/report-EE1.php");
                }
                else if($menu=="REPORTEE1SORT"){
                    include("view/report1.php");
                }
                else if($menu=="REPORTEE2FULL"){
                    include("view/report-EE2.php");
                }
                 else if($menu=="REPORTEE2SORT"){
                    include("view/report2.php");
                }
                else if($menu=="REPORTEE3FULL"){
                    include("view/report-EE3.php");
                }
                else if($menu=="REPORTEE3SORT"){
                    include("view/report3.php");
                }
                
                else if($menu=="NEWDETAIL2"){
                    include("view/insert-detail-lanjutan.php");
                }
                else if($menu=="REPORTALL"){
                    include("view/report-all.php");
                }
                
            }
            
            else{
                if($menu == "user")
                {
                    include("view/user-home.php");
                }
                else if($menu=="REGISKELAS"){
                    include("view/user-regisKelas.php");
                }
                else if($menu=="REGISKELASLAMA"){
                    include("view/user-regisKelasLama.php");
                }
                else if($menu=="CLASSINFO"){
                    include("view/user-classinfo.php");
                }
            }
            
            
            ?>
        
        
    

      </div><!-- /#page-wrapper -->


    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    
    

    <!-- Page Specific Plugins -->
    <script src="assets/js/raphael-min.js"></script>
    <script src="assets/js/morris-0.4.3.min.js"></script>
    <script src="assets/js/morris/chart-data-morris.js"></script>
    <script src="assets/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="assets/js/tablesorter/tables.js"></script>

  </body>
</html>



